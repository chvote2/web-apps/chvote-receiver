# Vote Receiver
[![pipeline status](https://gitlab.com/chvote2/web-apps/chvote-receiver/badges/master/pipeline.svg)](https://gitlab.com/chvote2/web-apps/chvote-receiver/commits/master)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=chvote-receiver&metric=alert_status)](https://sonarcloud.io/dashboard?id=chvote-receiver)

The Vote Receiver application aims to receive votes of the citizens during the voting period.

# Documentation

- Design documentation : [Vote Receiver Application Design](https://gitlab.com/chvote2/web-apps/chvote-receiver/builds/artifacts/master/raw/vote-receiver-design-docs/target/generated-docs/pdf/vote-receiver-application-design.pdf?job=artifact%3Adocs)
- [Backend tests report](https://gitlab.com/chvote2/web-apps/chvote-receiver/builds/artifacts/master/raw/backend/vr-backend-rest/target/site/surefire-report.html)
- [End to end tests report](https://gitlab.com/chvote2/web-apps/chvote-receiver/-/jobs/artifacts/master/download?job=check%3Ae2e)

# Components

Technicaly the project is composed of the following projects:

| Project name        | Type           | Description  |
| ------------- |-------------| -----|
| **Backend** | a multi-modules maven project | a java application bootstrapped by spring-boot framework. It exposes a REST API for the front-end part |
| **Deploy** | standard directory | All yaml files describing deployment instructions used to deploy to Kubernetes  |
| **Front-end** | Angular app| The Vote Receiver UI, based on Angular and Material design     |
| **Receiver-docker** | standard directory | It consists of a dockerfile for the nginx image, the nginx config file and a proxy config file. All used to configure Vote Receiver reverse-proxy component|
| **System-tests** | standard directory | Contains docker-compose definition file used in End to End testing of the Vote Receiver|
| **Vote-receiver-design-docs** | Maven project | It contains the overall project documentation: an *asciidoc* file and *plantuml* diagrams.|

Please refer to the backend and front-end respectives READMEs for further information:
* [Backend README](backend/README.md)
* [Front-end README](frontend/README.md)
   
# Building

## Pre-requisites
In order to build the project please follow instructions on the relevant README file (backend & front-end).

The default `java_home` variable must refer to a valid `JDK 11` installation.

## Build steps

* Build the backend application. See backend/README.md
* Build the frontend application. See frontend/README.md

# Running
Those are ordered steps to be followed in order to have the project running as a local isolated application:

##### 1. RabbitMQ

Communication between PACT and Vote Receiver is done via a RabbitMQ exchange (e.g. used to trigger operation 
initialization or destruction).

The `docker-compose.yml` file containing this configuration is under backend REST part.

To start it just run this command:

```bash
docker-compose -f backend/vr-backend-rest/docker-compose.yml up  
```

##### 2. Backend REST

The backend part can be started using this command:

```bash
cd backend/vr-backend-rest
SPRING_PROFILES_ACTIVE=dev,mocked-protocol mvn spring-boot:run
```

##### 3. Mock server

The backend part can be started using this command:

```bash
cd backend/vr-backend-mock-server/vr-backend-mock-server-rest
SPRING_PROFILES_ACTIVE=dev,mocked-protocol mvn spring-boot:run
```
Now open the browser at: [http://localhost:57646/mock/swagger-ui.html](http://localhost:57646/mock/swagger-ui.html)

Go to the `database-controller` entry then `create-use-case` in order to create an operation.

Only the following parameters are supported for now:
- `canton`: drop-down list of cantons. 
- `dateType`: specify if it's a current, past or future operation
- `groupedVotation`: boolean to choose a display mode in case of multiple votations
- `type`: the operation type
- `withElection`: boolean that must be setted to `false` as it's not yet supported
- `withVotation`: boolean that should be setted to `true`. Otherwise you will have an empty operation 

Here is an example of typical request url when clicking `Execute` button:

```bash
http://localhost:57646/mock/create-use-case?canton=GE&dateType=NOW&groupedVotation=true&type=TEST&withElection=false&withVotation=true
```

As a response, you will get something like that:

```json
{
  "protocolInstanceId": "17faec11-e806-4a1c-bf8e-df281ddca395",
  "votingCards": [
    {
      "voterId": 1,
      "cardNumber": "gHRPFzyBMQg0p_0DPaMOtULD3cS",
      "confirmationCode": "dcS98nOPnyuiP4uCEgJPHgzJGQx",
      "finalizationCode": "ehj",
      "verificationCodes": [
        "acc",
        "eKq",
        "aiq",
        "eUq",
        ...
```

You will seed those information to test the created operation. See: [Using the application](#5-using-the-application).

##### 4. Application front-end

Now that:
1. the backend toolchain is up,
2. an operation is created with mock server

you can start the front-end application:

```bash
cd frontend
npm start
```

Open the browser at: [http://localhost:4400/test/ge/fr](http://localhost:8080/test/)

##### 5. Using the application
Here is a typical scenario:

1. Click on the canton's logo for which the operation was created
2. Click on the operation (created with mock server)
3. In this `Identification` page you should enter an identification code.
    It's `voterId`+`:`+`cardNumber`.
    
    ```bash
    1:gHRPFzyBMQg0p_0DPaMOtULD3cS
    ```
    Then `continue`
    
4. It's a `Legal information` reminder
5. In this `Votation Ballot` you can make your choices (votes)
6. To submit the ballot, enter this birthdate `25.08.1980` and check the checkbox.
7. This is the `confirmation` page where you must put the `confirmationCode`. 
   From the previous response body it will look like:
    ```bash
    dcS98nOPnyuiP4uCEgJPHgzJGQx
    ```
    Then `Vote`

8. The voting ballot is submitted. Now verify that the returned code matchs the `finalizationCode`

# Contributing
See [CONTRIBUTING.md](https://gitlab.com/chvote2/documentation/chvote-docs/blob/master/CONTRIBUTING.md)

# License
This application is Open Source software released under the [Affero General Public License 3.0](LICENSE) 
license.