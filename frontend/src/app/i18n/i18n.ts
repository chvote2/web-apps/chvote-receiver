/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

import { i18n_global } from './global.i18n';

let baseI18N = {
  de: {
    "global": i18n_global.de,
  },
  fr: {
    "global": i18n_global.fr,
  },
  it: {
    "global": i18n_global.it,
  },
  rm: {
    "global": i18n_global.rm,
  }
};

function autoPrefixMissingTranslationByLang(frTranslations: any, otherTranslations: any, lang: string) {
  if (typeof frTranslations === 'string') {
    if (otherTranslations) {
      return otherTranslations;

    }

    // TODO : log missing translation when we will have most of them
    return lang.toUpperCase() + " - " + frTranslations
  }

  return Object.keys(frTranslations).reduce(
    (objectInCreation, newKey) => {
      objectInCreation[newKey] = autoPrefixMissingTranslationByLang(
        frTranslations[newKey],
        (otherTranslations || {})[newKey], lang);
      return objectInCreation;
    }, {});
}

function autoPrefixMissingTranslation(translations, langs: string[]) {
  let frTranslations = translations.fr;
  let toReturn = {fr: frTranslations};
  langs.forEach(lang => {
    let otherTranslations = translations[lang];
    toReturn[lang] = autoPrefixMissingTranslationByLang(frTranslations, otherTranslations, lang);
  });
  return toReturn;
}

export const i18n = autoPrefixMissingTranslation(
  baseI18N, ['de', 'it', 'rm']
);

export function deepMergeTranslations(translation1: any, translation2: any) {
  if (translation2 && !translation1) {
    return translation2;
  }
  if (translation1 && !translation2) {
    return translation1;
  }
  if (typeof translation2 === 'string') {
    return translation2;
  }

  return Array.from(new Set(Object.keys(translation1).concat(Object.keys(translation2))))
    .reduce((merged, key) => {
      merged[key] = deepMergeTranslations(translation1[key], translation2[key]);
      return merged;
    }, {});

}
