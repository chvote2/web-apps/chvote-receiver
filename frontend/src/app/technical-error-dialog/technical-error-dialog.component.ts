/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

import { Component, Inject } from "@angular/core";
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-technical-error-dialog',
  templateUrl: './technical-error-dialog.component.html',
  styleUrls: ['./technical-error-dialog.component.scss']
})
export class TechnicalErrorDialogComponent {
  public stopBackgroundRequest = new BehaviorSubject<boolean>(false);

  constructor(@Inject(MAT_DIALOG_DATA) public data: any, dialogRef: MatDialogRef<TechnicalErrorDialogComponent>) {
    dialogRef.afterOpen().subscribe(() => this.stopBackgroundRequest.next(true));
    dialogRef.afterClosed().subscribe(() => this.stopBackgroundRequest.next(false))
  }

  get message() {
    return this.data.message || 'global.technical-error-dialog.content';
  }

}
