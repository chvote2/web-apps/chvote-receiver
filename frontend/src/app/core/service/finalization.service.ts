/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FinalizationLogData } from '../../model/finalization-log-data';
import { Observable } from 'rxjs';
import { HttpParameters } from '../http/http.parameters.service';
import { CurrentVotingService } from '../../operation/current-voting-service';
import { tap } from 'rxjs/operators';

/**
 * Services used to handle finalization logs
 */
@Injectable()
export class FinalizationService {
  private readonly serviceUrl: string;

  constructor(private http: HttpClient,
              private httpParameters: HttpParameters,
              private currentVotingService: CurrentVotingService) {
    this.serviceUrl = this.httpParameters.apiBaseURL;
  }

  /**
   * Fetch the finalization log linked to the given protocol and voter and
   * store it in the current voting service.
   *
   * @param protocolInstanceId the protocol instance's ID
   * @param voterId            the voter's unique ID
   *
   * @return an observable to the corresponding finalization log or an observable to undefined if none is found
   */
  fetchFinalizationLog(protocolInstanceId: string, voterId: string): Observable<FinalizationLogData> {
    return this.http.get<FinalizationLogData>(`${this.serviceUrl}/finalization`,
      {params: {protocolInstanceId: protocolInstanceId, voterId: voterId}}).pipe(
      tap(finalizationLog => this.currentVotingService.attachFinalizationLogData(finalizationLog))
    );
  }

  /**
   * Log a finalization voteand store it in the current voting service.
   *
   * @param protocolInstanceId the protocol instance's ID
   * @param voterId            the voter's unique ID
   * @param code               the finalization code
   *
   * @return an observable to the newly created finalization log
   */
  logFinalization(protocolInstanceId: string, voterId: string, code: string): Observable<FinalizationLogData> {
    return this.http.post<FinalizationLogData>(
      `${this.serviceUrl}/finalization/log`, code, {params: {protocolInstanceId: protocolInstanceId, voterId: voterId}})
      .pipe(tap(finalizationLog => this.currentVotingService.attachFinalizationLogData(finalizationLog))
    );
  }
}
