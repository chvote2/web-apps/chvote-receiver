/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

import { Component, Input } from "@angular/core";
import { TranslateService } from '@ngx-translate/core';
import { Votation } from '../../../model/votation';
import { ElectionBallot } from '../../../model/election';
import { CurrentVotingService } from '../../current-voting-service';
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: '[single-ballot-card]',
  templateUrl: './single-ballot-card.component.html',
  styleUrls: ['./single-ballot-card.component.scss', '../shared.scss']
})

export class SingleBallotCardComponent {
  @Input() type: "votation" | "election-proportional" | "election-majority";
  @Input() ballot: Votation | ElectionBallot;

  constructor(private router: Router,
              private route: ActivatedRoute,
              private translateService: TranslateService,
              private currentVotingService: CurrentVotingService) {
  }

  get description(): string {
    let count = {counter: this.count};
    if (this.isVotation) {
      return this.translateService.instant('ballot-list.votation.ballot.summary', count)
    }
    if (this.type === "election-proportional") {
      return this.translateService.instant('ballot-list.election.ballot.summary.proportional', count);
    }
    return this.translateService.instant('ballot-list.election.ballot.summary.majority', count);
  }

  get isVotation(): boolean {
    return this.type === "votation";
  }

  private get count() {
    return this.isVotation ?
      (<Votation>this.ballot).subjects.length + (<Votation>this.ballot).variantSubjects.length :
      (<ElectionBallot>this.ballot).elections.reduce((pv, cv) => pv + cv.numberOfSelections, 0);
  }

  get documentations() {
    return this.ballot.documentations.filter(
      doc => doc.lang.toLowerCase() === this.translateService.currentLang);
  }

  get ballotFilled(): boolean {
    return this.currentVotingService.hasVotedFor(this.ballot);
  }

  participate(): void {
    if (this.isVotation) {
      CurrentVotingService.initVotationBallot([this.ballot as Votation]);
      this.currentVotingService.lastVisitedPage = 'BALLOT_SELECTION';
      this.router.navigate(['../votationBallot/'],
        {relativeTo: this.route, queryParams: {domainOfInfluence: this.ballot.domainOfInfluence}});
    } else {
      // TODO: navigate to election ballot page
    }
  }
}

