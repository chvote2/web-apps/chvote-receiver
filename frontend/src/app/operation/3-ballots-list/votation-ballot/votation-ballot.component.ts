/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

import { Component, OnInit } from "@angular/core";
import { CurrentVotingService } from "../../current-voting-service";
import { ActivatedRoute, Router } from "@angular/router";
import { Step, VoteWorkflowViewComponent } from "../../vote-workflow-display/vote-workflow-view.component";
import { Votation } from "../../../model/votation";
import { ChvoteDialogComponent } from '../../../core/chvote-dialog/chvote-dialog.component';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'votation-ballot, [votation-ballot]',
  templateUrl: './votation-ballot.component.html',
  styleUrls: ['./votation-ballot.component.scss']
})

export class VotationBallotComponent implements OnInit {

  ballots: Votation[];

  constructor(private router: Router,
              private route: ActivatedRoute,
              private voteWorkflowViewComponent: VoteWorkflowViewComponent,
              private currentVotingService: CurrentVotingService,
              private dialog: MatDialog) {
  }

  ngOnInit() {
    this.initBallots();
  }

  get currentStepNumber() {
    return this.voteWorkflowViewComponent.getStepNumber(Step.BallotList);
  }

  get nextStepNumber() {
    return this.voteWorkflowViewComponent.getStepNumber(Step.Summary);
  }

  get singleBallot(): boolean {
    return this.currentVotingService.hasSingleBallot();
  }

  quit(): void {
  }

  back(): void {
    if (CurrentVotingService.hasVotingBallotChanged(this.ballots)) {
      // display confirmation message
      this.dialog.open(ChvoteDialogComponent, {
        width: '500px',
        disableClose: true,
        data: {
          messageKey: 'ballot-list.dialog.confirm-cancel',
          primaryButtonKey: 'global.actions.yes',
          secondaryButtonKey: 'global.actions.no'
        }
      }).afterClosed().subscribe((accept: boolean) => {
        if (accept) {
          this.router.navigate(['../ballotsList'], {relativeTo: this.route});
        }
      });
    } else {
      this.router.navigate(['../ballotsList'], {relativeTo: this.route});
    }
  }

  validate(): void {
    CurrentVotingService.commitVotationBallot(this.ballots);
    if (this.singleBallot || this.currentVotingService.lastVisitedPage === 'SUMMARY') {
      this.router.navigate(['../summary'], {relativeTo: this.route});
    } else {
      this.router.navigate(['../ballotsList'], {relativeTo: this.route});
    }
  }

  private initBallots() {
    let domainOfInfluence = this.route.snapshot.queryParams.domainOfInfluence;
    if (domainOfInfluence) {
      this.ballots = [
        this.currentVotingService.votations.find(votation => votation.domainOfInfluence === domainOfInfluence)
      ];
    } else {
      this.ballots = this.currentVotingService.votations;
    }
  }
}
