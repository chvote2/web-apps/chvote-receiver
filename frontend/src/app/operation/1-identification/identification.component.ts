/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Component, OnInit } from '@angular/core';
import { TranslateService } from "@ngx-translate/core";
import { ActivatedRoute, Router } from "@angular/router";
import { IdentificationService } from './identification.service';
import { Step, VoteWorkflowViewComponent } from '../vote-workflow-display/vote-workflow-view.component';
import { CurrentVotingService } from '../current-voting-service';
import { HttpRequestErrorProvider } from '../../core/http/ErrorInterceptor';
import { focusOnError } from '../../core/util/form-utils';
import { ProtocolService } from '../../core/service/protocol.service';
import { FinalizationService } from '../../core/service/finalization.service';
import { Documentation } from '../../model/documentation';
import { HttpParameters } from '../../core/http/http.parameters.service';
import { ChvoteDialogComponent } from '../../core/chvote-dialog/chvote-dialog.component';
import { MatDialog } from '@angular/material';
import { DocumentationService } from '../../core/service/documentation.service';

@Component({
  templateUrl: './identification.component.html',
  styleUrls: ['./identification.component.scss']
})

export class IdentificationComponent implements OnInit {

  currentStepNumber;
  nextStepNumber;
  identificationForm: FormGroup;

  constructor(private httpParams: HttpParameters,
              private router: Router,
              private route: ActivatedRoute,
              private translateService: TranslateService,
              private formBuilder: FormBuilder,
              private voteWorkflowViewComponent: VoteWorkflowViewComponent,
              private identificationService: IdentificationService,
              private finalizationService: FinalizationService,
              private currentVotingService: CurrentVotingService,
              private protocolService: ProtocolService,
              private dialog: MatDialog,
              private documentationService: DocumentationService) {
    this.createForm();

    // initialize protocol client service
    this.protocolService.initialize(this.currentVotingService.operation.protocolInstanceId);
  }


  ngOnInit() {
    this.currentStepNumber = this.voteWorkflowViewComponent.getStepNumber(Step.Identification);
    this.nextStepNumber = this.voteWorkflowViewComponent.getStepNumber(Step.LegalInformation);
    this.currentVotingService.lastVisitedPage = 'IDENTIFICATION';
  }

  private createForm() {
    this.identificationForm = this.formBuilder.group({
      cardNumber: [
        null,
        [
          Validators.required,
          Validators.minLength(1)
        ]
      ]
    });
  }

  /**
   * @return true if certification documentation is defined for the operation
   */
  get hasCertificateDocumentation(): boolean {
    return this.certificateDocumentation !== undefined;
  }

  /**
   * @return the certification documentation URL
   */
  get certificateDocumentationUrl(): string {
    return this.documentationService.getDocumentationUrl(this.certificateDocumentation.fileIndex);
  }

  quit(): void {
  }

  continue(): void {
    this.identificationForm.controls.cardNumber.markAsTouched();
    this.identificationForm.updateValueAndValidity();

    if (this.identificationForm.valid) {

      let voterIdAndCardNumber = this.identificationForm.getRawValue().cardNumber.split(':');
      let voterId = voterIdAndCardNumber[0];
      let cardNumber = voterIdAndCardNumber[1];

      // check for finalization log
      this.finalizationService.fetchFinalizationLog(
        this.currentVotingService.operation.protocolInstanceId, voterId).subscribe(finalizationLog => {
        if (finalizationLog) {
          // user has already voted using the internet channel, redirect to the finalization page
          this.router.navigate(['../finalization'], {relativeTo: this.route});

        } else {
          // proceed with identification
          this.identify(voterId, cardNumber);
        }
      });

    } else {
      focusOnError(this.identificationForm)
    }
  }

  private identify(voterId: string, cardNumber: string) {
    this.identificationService.identify(this.currentVotingService.operation.protocolInstanceId,
      // TODO replace following commented line after implementation recovery VoterId
      // cardNumber
      voterId
    )
      .subscribe(() => {
        // save card number for ballot encryption
        this.protocolService.identificationCredentials = cardNumber;
        this.protocolService.voterId = voterId;
        // navigate to legal information page
        this.router.navigate(['../legalInformation'], {relativeTo: this.route});
      }, (errorProvider: HttpRequestErrorProvider) => {

        errorProvider
          .onValidationError(/.*(BirthDateCheck|ConfirmationCheck|VerificationCodeRevealed)Exception/, error => {
            this.showErrorDialog(error, 'global.dialogs.title.warning');
          })
          .onValidationError(/.*/, error => {
            this.showErrorDialog(error, 'identification.error-dialog.title');
        });

      });


  }

  private showErrorDialog(error, titleKey) {
    let messageParams = {};
    error.globalErrors[0].messageParams.forEach((param, index) => messageParams["param" + index] = param)

    this.dialog.open(ChvoteDialogComponent, {
      width: '500px',
      disableClose: true,
      data: {
        type: 'warning',
        titleKey: titleKey,
        messageKey: `identification.error-dialog.${error.globalErrors[0].messageKey}`,
        messageParams: messageParams,
        primaryButtonKey: 'global.actions.close'
      }
    });
  }

  /**
   * @return the documentation explaining certificate verification; try to get the document in
   * the selected language, use operation's default language if not found and finally return
   * "undefined" if no documentation match
   */
  private get certificateDocumentation(): Documentation {
    // search based on selected lang
    let docs: Documentation[] = this.currentVotingService.operation.documentations.filter(doc => {
      return doc.type === 'CERTIFICATES' &&
             doc.lang.toLowerCase() === this.translateService.currentLang;
    });

    // if not found, search based on default lang
    if (docs.length === 0) {
      docs = this.currentVotingService.operation.documentations.filter(doc => {
        return doc.type === 'CERTIFICATES' &&
               doc.lang.toLowerCase() === this.currentVotingService.operation.defaultLang.toLowerCase();
      });
    }

    return docs.length > 0 ? docs[0] : undefined;
  }
}
