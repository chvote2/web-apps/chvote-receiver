/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { tap } from 'rxjs/operators';
import { HttpParameters } from '../../core/http/http.parameters.service';
import { CurrentVotingService } from '../current-voting-service';
import { VoterOperationData } from '../../model/voter-operation-data';
import { Observable } from 'rxjs';

/**
 * This class contains elements concerning a identification
 */
@Injectable()
export class IdentificationService {
  private serviceUrl: string;

  constructor(private http: HttpClient,
              private httpParameters: HttpParameters,
              private currentVotingService: CurrentVotingService
  ) {
    this.serviceUrl = this.httpParameters.apiBaseURL;
  }

  identify(protocolInstanceId: string, voterId: string): Observable<VoterOperationData> {
    return this.http.get<VoterOperationData>(`${this.serviceUrl}/identification`,
      {params: {protocolInstanceId: protocolInstanceId, voterId: voterId}}).pipe(
      tap(
        voterOperationData => {
          this.currentVotingService.attachVoterOperationData(voterOperationData);
        }
      )
    );
  }


}
