/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { Step, VoteWorkflowViewComponent } from '../vote-workflow-display/vote-workflow-view.component';
import { CurrentVotingService } from '../current-voting-service';

@Component({
  templateUrl: './legal-information.component.html',
  styleUrls: ['./legal-information.component.scss']
})
export class LegalInformationComponent implements OnInit {
  currentStepNumber;
  nextStepNumber;

  constructor(private router: Router,
              private route: ActivatedRoute,
              private voteWorkflowViewComponent: VoteWorkflowViewComponent,
              private currentVotingService: CurrentVotingService) {
  }

  ngOnInit() {
    this.currentStepNumber = this.voteWorkflowViewComponent.getStepNumber(Step.LegalInformation);
    this.nextStepNumber = this.voteWorkflowViewComponent.getStepNumber(Step.BallotList);
    this.currentVotingService.lastVisitedPage = 'LEGAL_INFORMATION';
  }

  quit(): void {

  }

  continue(): void {
    if (this.currentVotingService.hasSingleBallot()) {
      // there is only one ballot available, route directly to the ballot page
      if (this.currentVotingService.elections.length == 0) {
        // votation ballot case
        CurrentVotingService.initVotationBallot(this.currentVotingService.votations);
        this.router.navigate(['../votationBallot'], {relativeTo: this.route});
      } else {
        // TODO: election ballot case
      }
    } else {
      // there are multiple ballots, route to the ballot selection page
      this.router.navigate(['../ballotsList'], {relativeTo: this.route});
    }
  }
}
