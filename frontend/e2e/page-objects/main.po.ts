/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

import { browser, by, ElementFinder, ProtractorBrowser } from 'protractor';
import { promise } from 'selenium-webdriver';
import { log } from 'util';
import * as axe from 'axe-core';
import { Button } from '../shared/button';
import controlFlow = promise.controlFlow;

let btoa = require('btoa');

/**
 * Main page object containing utility functions used by all pages.
 */
export class Main {

  private static takeScreenshot = require('../pmsr/protractor-multi-screenshots-reporter.js').takeScreenshot;
  private static axeBuilder = require('axe-webdriverjs');

  /**
   * @returns the current step title element
   */
  static get stepTitle(): ElementFinder {
    return browser.element(by.id('stepTitle'));
  }

  /**
   * @returns the current step title text
   */
  static get stepTitleText(): promise.Promise<string> {
    return this.stepTitle.getText();
  }

  /**
   * @returns the quit button
   */
  static get quitButton(): Button {
    return new Button('quit-button');
  }

  /**
   * @returns the next step button
   */
  static get nextStepButton(): Button {
    return new Button('next-step-button');
  }

  static get globalError(){
    return browser.element(by.id("global-error"))
  }


  /**
   * Check page's accessibility.
   *
   * @param disabledRules (optional) rules to  disable
   */
  static checkAccessibility(...disabledRules: string[]) {
    it('should have no accessibility issues', () => {
      this.axeBuilder(browser.driver)
        .disableRules([...disabledRules, 'region'])
        .analyze(function (results: axe.AxeResults) {
          results.violations.forEach(violation => {
            fail(`
<a href="data:text/json;charset=utf-8;base64,${btoa(JSON.stringify(violation, undefined, 2))}"  target="_blank" rel="noopener">
    ${violation.id} - ${violation.description}
</a>`
            );
          });
        });
    })
  }

  /**
   * Navigate to the given page
   *
   * @param path {string} the destination path
   * @param {ProtractorBrowser} b the browser instance (default to current Protractor Browser)
   * @return {promise.Promise<boolean>} is true if the url has been actually changed
   */
  static navigateTo(path: string, b: ProtractorBrowser = browser): promise.Promise<boolean> {
    return controlFlow().execute(() => {
      return b.getCurrentUrl().then((url) => {
        if (url !== b.baseUrl + path) {
          log(`navigate to ${path}`);
          b.get(path);
          return true;
        } else {
          log(`navigate to ${path} cancelled already here`);
          return false;
        }
      });
    });
  }
}


