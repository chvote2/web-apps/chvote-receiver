/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

import { Button } from '../shared/button';
import { by, element, ElementFinder } from 'protractor';
import { promise } from 'selenium-webdriver';
import { ChvoteCheckbox } from '../shared/chvote-checkbox';
import { InputField } from '../shared/input-field';

/**
 * Page object for the summary page.
 */
export class SummaryPage {

  /**
   * @return the birth date input
   */
  static get birthDateInput() {
    return new InputField(element(by.tagName("summary")), "birthDate")
  }

  /**
   * @returns the button to modify the unique ballot
   */
  static get modifyButton(): Button {
    return this.getModifyButton(0);
  }

  /**
   * Retrieve a ballot's cancel participation button
   *
   * @param ballotIndex the index of the ballot
   * @returns the button to cancel the participation to a given ballot
   */
  static getCancelParticipateButton(ballotIndex: number): Button {
    return new Button(this.getBallotCard(ballotIndex).element(by.className('cancel-button')));
  }

  /**
   * Retrieve a ballot's modify button
   *
   * @param ballotIndex the index of the ballot
   * @returns the button to modify a given ballot
   */
  static getModifyButton(ballotIndex: number): Button {
    return new Button(this.getBallotCard(ballotIndex).element(by.className('modify-button')));
  }

  /**
   * Retrieve an empty ballot's description
   *
   * @param ballotIndex the index of the ballot
   * @returns the ballot's description is the ballot is empty
   */
  static getEmptyBallotDescription(ballotIndex: number): ElementFinder {
    return this.getBallotCard(ballotIndex).element(by.className('empty-ballot'));
  }

  /**
   * Get the displayed answer for a given subject
   *
   * @param globalIndex subject's global index
   * @return the corresponding answer
   */
  static getAnswer(globalIndex: string): promise.Promise<string> {
    return element(by.id(`summary-answer-${globalIndex}`)).getText();
  }

  /**
   * @return a reference to the acceptation checkbox
   */
  static get acceptCheckbox(): ChvoteCheckbox {
    return new ChvoteCheckbox('checkNextStep');
  }

  /**
   * @returns the button to go back to ballot's selection page
   */
  static get backButton(): Button {
    return new Button('back-button');
  }

  /**
   * @return true if the summary is currently present, false otherwise
   */
  static isPresent(): promise.Promise<boolean> {
    return element(by.tagName("summary")).isPresent();
  }

  private static getBallotCard(ballotIndex: number): ElementFinder {
    return element.all(by.tagName('chvote-votation-ballot')).get(ballotIndex);
  }
}



