/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

import { browser, by, element, ElementFinder, ExpectedConditions, promise } from 'protractor';

/**
 * Page object for the CHVote Dialog.
 */
export class ChvoteDialog {

  /**
   * @return the CHVote Dialog
   */
  static get dialog(): ElementFinder {
    return element(by.tagName('chvote-dialog'));
  }

  /**
   * @returns the dialog's message
   */
  static get message(): promise.Promise<string> {
    return ChvoteDialog.dialog.element(by.className('mat-dialog-content')).getText();
  }

  static waitForMessage() {
    browser.wait(ExpectedConditions.visibilityOf(ChvoteDialog.dialog.element(by.className('mat-dialog-content'))), 5000,
      "Message should be present on the displayed popup");
  }


  /**
   * Click on the dialog's primary button and wait for dialog's close
   */
  static clickPrimary(): void {
    ChvoteDialog.dialog.element(by.id('dialog-primary-button')).click();
    browser.wait(ExpectedConditions.not(ExpectedConditions.visibilityOf(ChvoteDialog.dialog)));
  }

  /**
   * Click on the dialog's secondary button and wait for dialog's close
   */
  static clickSecondary(): void {
    ChvoteDialog.dialog.element(by.id('dialog-secondary-button')).click();
    browser.wait(ExpectedConditions.not(ExpectedConditions.visibilityOf(ChvoteDialog.dialog)));
  }
}



