/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

import { browser, promise, protractor } from 'protractor';
import { log } from 'util';

const request = require("request");

function doRequest(path: string) {
  let deferred = protractor.promise.defer<any>();
  let url = `http://${browser.params.mockServer.hostname}:${browser.params.mockServer.port}/${path}`;
  request(url,
    {json: true, proxy: ""},
    (err, res, body) => {
      if (err) {
        return console.log(err);
      }
      deferred.fulfill(body);
    });

  return deferred.promise;
}

export function resetDB() {
  log('resetting database');
  return doRequest('/mock/database/reset');
}

export enum Canton {
  GE = "GE", AG = "AG", SG = "SG", BE = "BE", BS = "BS", LU = "LU"
}

export enum TestCaseType {
  TEST = "TEST",
  SIMULATION = "SIMULATION",
  REAL = "REAL"
}

export enum DateType {
  PAST = "PAST",
  NOW = "NOW",
  FUTURE = "FUTURE"
}

export interface VotingCard {
  voterId: number,
  cardNumber: string,
  confirmationCode: string,
  finalizationCode: string,
  verificationCodes: string[]
}

export interface UseCaseMetaData {
  protocolInstanceId: string,
  votingCards: VotingCard[]
}

export function createUseCase(election = false,
                              votation = true,
                              type: TestCaseType = TestCaseType.TEST,
                              canton: Canton = Canton.GE,
                              groupedVotation = true,
                              dateType: DateType = DateType.NOW): promise.Promise<UseCaseMetaData> {
  log(
    `create use case: withElection=${election}, withVotation=${votation}, type=${type}, canton=${canton}, groupedVotation=${groupedVotation}, dateType=${dateType}`);
  return doRequest(
    `mock/create-use-case?withElection=${election}&withVotation=${votation}&type=${type}&canton=${canton}&groupedVotation=${groupedVotation}&dateType=${dateType}`);
}


export function burnVotingCard(protocolInstanceId: string, voterId: string,
                               type: 'AUTHENTICATION' | 'CONFIRMATION' | 'NONE') {
  return doRequest(`mock/voting-card/burn?protocolId=${protocolInstanceId}&voterIndex=${voterId}&burningType=${type}`);
}

export function setUserStatusInVrum(protocolInstanceId: string,
                                    voterId: string,
                                    status: "READY_TO_VOTE" | "ALREADY_VOTED" | "VERIFICATION_CODE_REVEALED") {
  return doRequest(`mock/vrum/set-status?protocolInstanceId=${protocolInstanceId}&voterId=${voterId}&status=${status}`);
}
