/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

import { by, element, ElementFinder } from 'protractor';
import { promise } from 'selenium-webdriver';

/**
 * Object used to manipulate a CHVote checkbox.
 */
export class ChvoteCheckbox {

  private field: ElementFinder;
  private labelField: ElementFinder;
  private selected: boolean = false;

  constructor(id: string) {
    this.field = element(by.id(`input-${id}`));
    this.labelField = element(by.id(`label-${id}`));
    this.checked.then(checked => {
      this.selected = checked;
    });
  }

  /**
   * @returns {promise.Promise<boolean>} whether the field is checked or not
   */
  get checked(): promise.Promise<boolean> {
    return this.field.isSelected();
  }

  /**
   * Performs a click on the field
   */
  click() {
    this.labelField.click();
  }
}
