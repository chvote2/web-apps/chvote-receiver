/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

let fileSystem = require('fs');
let fs = require('fs-extra');
let _ = require('lodash');
let activeScreenshots = !global.disableScreenshots;

function readFile(filename) {
  return fileSystem.readFileSync(filename, 'utf-8');
}

function formatTime(time) {
  time = time / 1000;
  let hours = Math.floor(time / 3600);
  let minutes = Math.floor(time % 3600 / 60);
  let seconds = (time % 3600) % 60;
  return hours + 'h ' + minutes + 'min ' + seconds + 's';
}

class Reporter {
  constructor() {
    this.current = {
      suites: [],
      specs: []
    };


    this.pending = 0;
    this.passed = 0;
    this.failed = 0;
    this.id = 0;
    this.screenshotsCount = 0;
  }


  suiteStarted(result) {
    result.description.split("/").forEach(descriptionPart => this._suiteStarted(descriptionPart));
    console.log('\x1b[35m%s\x1b[0m', this.suiteToString());
  };

  _suiteStarted(description) {
    let suite = this.current.suites.find(s => s.name === description);
    if (!suite) {
      suite = {
        id: this.id++,
        name: description,
        suites: [],
        specs: [],
      };
      this.current.suites.push(suite);
    }
    suite.parent = this.current;
    this.current = suite;
  }

  suiteDone(result) {
    result.description.split("/").forEach(descriptionPart => this._suiteDone());
    this.complete("WIP", "In progress")
  }

  _suiteDone() {
    let suite = this.current;

    if (suite.status === "failed") {
      suite.parent.status = "failed";
    }
    this.current = suite.parent;
    delete suite.parent; // To be able to do JSON.stringify
  };


  specStarted(result) {
    this.current.specs.push({name: result.description, screenshots: [], id: this.id++});
    return this.takeScreenshots("Start of the test case")
  };

  takeScreenshots(title) {
    if (activeScreenshots) {
      let relativeFileName = 'screenshots/' + this.screenshotsCount++ + '.png';
      let filename = 'test_reports/pmsr/' + relativeFileName;
      this.currentSpec.screenshots.push({relativeFileName, title});

      return browser.takeScreenshot().then(png => {
        let stream = fs.createWriteStream(filename);
        stream.write(new Buffer(png, 'base64'));
        stream.end();
        return filename;
      });
    }
  };

  specDone(result) {
    this.currentSpec.passedExpectationsCount = result.passedExpectations.length;
    this.currentSpec.failedExpectations = result.failedExpectations;
    this.currentSpec.failedExpectations.forEach(fe => fe.stack = fe.stack.replace("<", "&lt;").replace(">", "&gt;"));
    this.currentSpec.status = result.status;
    this.currentSpec.runningDate = new Date();

    if (result.status === "pending") {
      console.log('\x1b[33m%s\x1b[0m', "  " + this.currentSpec.name);
      this.pending++;
    }
    else if (result.status === "passed") {
      console.log('\x1b[32m%s\x1b[0m', "  " + this.currentSpec.name);
      this.passed++;
    }
    else if (result.status === "failed") {
      console.log('\x1b[31m%s\x1b[0m', "  " + this.currentSpec.name);
      this.failed++;
      this.current.status = "failed";
    }

    return this.takeScreenshots("End of the test case");
  };


  suiteToString() {
    let path = [];
    let cPath = this.current;

    while (cPath && cPath.name) {
      path.splice(0, 0, cPath.name);
      cPath = cPath.parent;
    }


    return path.join("/");
  }


  get currentSpec() {
    return this.current.specs[this.current.specs.length - 1];
  }

  prepare() {
    this.startTime = new Date().getTime();
    fs.emptyDir('test_reports/pmsr/screenshots');
    jasmine.getEnv().addReporter(this/*myReporter*/);

  };


  complete(browserName, browserVersion) {


    let drawSpecs = (specs) => {
      return _.template(readFile("e2e/pmsr/specs.html"))({
        _: _,
        specs: specs
      })
    };

    let drawSuites = (suites) => {
      return _.template(readFile("e2e/pmsr/suites.html"))({
        _: _,
        suites: suites,
        drawSuites: drawSuites,
        drawSpecs: drawSpecs,
      })
    };


    fileSystem.writeFileSync("test_reports/pmsr/index.html",
      _.template(readFile("e2e/pmsr/index.html"))({
        styles: readFile("e2e/pmsr/style.css"),
        pending: this.pending,
        passed: this.passed,
        failed: this.failed,
        browser: browserName,
        suites: this.current.suites,
        specs: this.current.specs,
        drawSuites: drawSuites,
        drawSpecs: drawSpecs,
        browserVersion: browserVersion,
        reportName: "CHVote Receiver tests report",
        timeElapsed: formatTime(new Date().getTime() - this.startTime)
      }));


  }
}

let reporter = new Reporter();

module.exports.prepareReporter = () => reporter.prepare();
module.exports.takeScreenshot = (title) => reporter.takeScreenshots(title);
module.exports.completeReport = (browserName, browserVersion) => reporter.complete(browserName, browserVersion);

