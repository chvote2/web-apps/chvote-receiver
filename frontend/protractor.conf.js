/*
 * #%L
 * chvote-receiver
 * %%
 * Copyright (C) 2016 - 2018 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

// Protractor configuration file, see link for more information
// https://github.com/angular/protractor/blob/master/lib/config.ts
let prepareReporter = require("./e2e/pmsr/protractor-multi-screenshots-reporter").prepareReporter;
let completeReport = require("./e2e/pmsr/protractor-multi-screenshots-reporter").completeReport;

exports.config = {
  allScriptsTimeout: 24000,
  suites: {
    "past-and-future-operation": './e2e/1-single-votation-ballot/past-and-future-operation.e2e-spec.ts',
    'verification-code-revealed': './e2e/1-single-votation-ballot/verification-code-revealed.e2e-spec.ts',
    'single-votation-ballot': './e2e/1-single-votation-ballot/single-votation-ballot.e2e-spec.ts',
    'multi-votation-ballot': './e2e/2-multi-votation-ballot/multi-votation-ballot.e2e-spec.ts'
  },
  exclude: ['**/*.po.ts'], // exclude page objects
  capabilities: {
    'browserName': 'chrome',
    'chromeOptions': {
      'args': ['no-sandbox', 'headless', 'disable-gpu', "window-size=1400,1500"],
      //'args': ['no-sandbox', 'disable-gpu', "window-size=1400,1500"],
    }
  },
  params: {
    mockServer: {
      hostname: "127.0.0.1",
      port: 57646
    }
  },
  directConnect: true,
  baseUrl: 'http://localhost:4400/test/vote/',
  framework: 'jasmine',
  jasmineNodeOpts: {
    showColors: true,
    defaultTimeoutInterval: 30000,
    print: function () {
    }
  },
  beforeLaunch: function () {
    require('ts-node').register({
      project: 'e2e/tsconfig.e2e.json'
    });
  },
  onPrepare() {
    prepareReporter();
  },
  onComplete: function () {
    browser.getCapabilities().then(function (caps) {
      let browserName = caps.get('browserName');
      let browserVersion = caps.get('version');
      completeReport(browserName, browserVersion);
    });
  }

};
