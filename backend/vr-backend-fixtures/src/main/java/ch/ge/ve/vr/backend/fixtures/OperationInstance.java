/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

package ch.ge.ve.vr.backend.fixtures;

import ch.ge.ve.vr.backend.repository.data.Election;
import ch.ge.ve.vr.backend.repository.data.Operation;
import ch.ge.ve.vr.backend.repository.data.Votation;
import java.util.List;
import java.util.Set;

public class OperationInstance {
  private final Operation          operation;
  private final List<Votation>     votations;
  private final List<Election>     elections;
  private final Set<FileReference> rawFiles;

  public OperationInstance(Operation operation, List<Votation> votations, List<Election> elections,
                           Set<FileReference> rawFiles) {
    this.operation = operation;
    this.votations = votations;
    this.elections = elections;
    this.rawFiles = rawFiles;
  }

  public Operation getOperation() {
    return operation;
  }

  public List<Votation> getVotations() {
    return votations;
  }

  public List<Election> getElections() {
    return elections;
  }

  public Set<FileReference> getRawFiles() {
    return rawFiles;
  }
}
