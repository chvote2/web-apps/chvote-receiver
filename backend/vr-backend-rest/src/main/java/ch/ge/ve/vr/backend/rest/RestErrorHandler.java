/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

package ch.ge.ve.vr.backend.rest;

import static java.util.stream.Collectors.toList;

import ch.ge.ve.vr.backend.rest.model.GlobalValidationErrorModel;
import ch.ge.ve.vr.backend.rest.model.TechnicalErrorModel;
import ch.ge.ve.vr.backend.rest.model.ValidationErrorModel;
import ch.ge.ve.vr.backend.service.exception.AccessSecurityException;
import ch.ge.ve.vr.backend.service.exception.BusinessException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Intercepts business validation errors, field validation errors and technical errors and translate them to HTTP status
 * code and DTO payload for interpretation by frontend application
 */
@ControllerAdvice
public class RestErrorHandler {

  private static final Logger logger                       = LoggerFactory.getLogger("vr-exceptions");
  private static final String ERROR_UNEXPECTED_MESSAGE_KEY = "global.errors.unexpected";

  @ExceptionHandler(AccessSecurityException.class)
  @ResponseStatus(HttpStatus.FORBIDDEN)
  @ResponseBody
  public void processSecurityError(AccessSecurityException ex) {
    logger.error("Access error", ex);
  }


  @ExceptionHandler(BusinessException.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ResponseBody
  public ValidationErrorModel processBusinessValidationError(BusinessException ex) {
    logger.error("BusinessValidationError", ex);
    ValidationErrorModel validationErrorModel = new ValidationErrorModel(ex.getClass().getName());
    GlobalValidationErrorModel globalError =
        new GlobalValidationErrorModel(ex.getMessageKey(), paramsObjectsToStringList(ex.getParameters()));
    validationErrorModel.addGlobalError(globalError);
    return validationErrorModel;
  }

  private List<String> paramsObjectsToStringList(Object[] parameters) {
    return parameters != null ? Arrays.stream(parameters).map(Object::toString).collect(toList()) :
        Collections.emptyList();
  }

  @ExceptionHandler(Throwable.class)
  @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
  @ResponseBody
  public TechnicalErrorModel processRuntimeError(Throwable t) {
    logger.error("RuntimeError", t);
    return new TechnicalErrorModel(HttpStatus.INTERNAL_SERVER_ERROR.value(), t.getClass().getName(),
                                   ERROR_UNEXPECTED_MESSAGE_KEY);
  }

}
