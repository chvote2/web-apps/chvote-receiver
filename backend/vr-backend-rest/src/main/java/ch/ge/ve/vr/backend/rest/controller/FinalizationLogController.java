/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

package ch.ge.ve.vr.backend.rest.controller;

import ch.ge.ve.vr.backend.rest.interceptor.CheckSiteIsOpen;
import ch.ge.ve.vr.backend.service.IdentificationService;
import ch.ge.ve.vr.backend.service.model.FinalizationLogData;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller to access and handle finalization logs
 */
@RestController
@RequestMapping("/finalization")
public class FinalizationLogController {

  private final IdentificationService identificationService;

  @Autowired
  public FinalizationLogController(IdentificationService identificationService) {
    this.identificationService = identificationService;
  }

  @GetMapping()
  @CheckSiteIsOpen
  public FinalizationLogData fetchLog(@RequestParam("protocolInstanceId") String protocolInstanceId,
                                      @RequestParam("voterId") int voterId,
                                      HttpServletRequest request) {
    return this.identificationService
        .fetchFinalizationLog(OperationController.TEST_CONTEXT.equals(request.getServletPath()),
                              protocolInstanceId, voterId);
  }

  @PostMapping(value = "/log")
  @CheckSiteIsOpen
  public FinalizationLogData log(@RequestBody String code,
                                 @RequestParam("protocolInstanceId") String protocolInstanceId,
                                 @RequestParam("voterId") int voterId) {
    return this.identificationService.logFinalization(protocolInstanceId, voterId, code);
  }

}
