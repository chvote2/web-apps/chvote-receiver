<?xml version="1.0" encoding="UTF-8"?>
<!--
  ~ /*
  ~  - #%L
  ~  - CHVote-Receiver
  ~  - %%
  ~  - Copyright (C) 2016 - 2018 République et Canton de Genève
  ~  - %%
  ~  - This program is free software: you can redistribute it and/or modify
  ~  - it under the terms of the GNU Affero General Public License as published by
  ~  - the Free Software Foundation, either version 3 of the License, or
  ~  - (at your option) any later version.
  ~  -
  ~  - This program is distributed in the hope that it will be useful,
  ~  - but WITHOUT ANY WARRANTY; without even the implied warranty of
  ~  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  ~  - GNU General Public License for more details.
  ~  -
  ~  - You should have received a copy of the GNU Affero General Public License
  ~  - along with this program. If not, see <http://www.gnu.org/licenses/>.
  ~  - #L%
  ~  */
  -->

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>2.1.1.RELEASE</version>
        <relativePath/> <!-- lookup parent from repository -->
    </parent>

    <groupId>ch.ge.ve.receiver</groupId>
    <artifactId>vr-backend-base</artifactId>
    <version>0.0.1</version>
    <packaging>pom</packaging>

    <name>vr-backend-base</name>
    <description>chvote vote receiver application project</description>

    <modules>
        <module>vr-backend-mock-server</module>
        <module>vr-backend-repository</module>
        <module>vr-backend-rest</module>
        <module>vr-backend-service</module>
        <module>vr-backend-fixtures</module>
    </modules>

    <distributionManagement>
        <snapshotRepository>
            <id>chvote-snapshots</id>
            <name>bitnami-artifactory-dm-df0d-snapshots</name>
            <url>http://artifactory.chvote-dev.work/artifactory/libs-snapshot</url>
        </snapshotRepository>
        <repository>
            <id>chvote</id>
            <name>bitnami-artifactory-dm-df0d-releases</name>
            <url>http://artifactory.chvote-dev.work/artifactory/libs-release</url>
        </repository>
    </distributionManagement>

    <properties>
        <build.timestamp>${maven.build.timestamp}</build.timestamp>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
        <java.version>11</java.version>
        <maven.compiler.source>${java.version}</maven.compiler.source>
        <maven.compiler.target>${java.version}</maven.compiler.target>
        <guava.version>27.0.1-jre</guava.version>
        <spock.version>1.2-groovy-2.5</spock.version>
        <swagger-annotations.version>2.0.5</swagger-annotations.version>
        <swagger-maven-plugin-version>3.1.7</swagger-maven-plugin-version>
        <objenesis.version>3.0.1</objenesis.version>
        <cglib-nodep.version>3.2.9</cglib-nodep.version>
        <jacoco-maven-plugin.version>0.8.2</jacoco-maven-plugin.version>
        <asm.version>7.0</asm.version>
        <bouncycastle.version>1.60</bouncycastle.version>
        <json.version>20180813</json.version>
        <javax-interceptor-api.version>1.2.2</javax-interceptor-api.version>
        <commons-fileupload.version>1.3.3</commons-fileupload.version>

        <!-- JAR versions -->
        <pact-b2b-client.version>0.0.5</pact-b2b-client.version>
        <chvote-protocol-core.version>1.0.16</chvote-protocol-core.version>
        <chvote-crypto.version>1.2.0</chvote-crypto.version>
        <model-converter.version>1.0.19</model-converter.version>
        <jackson-serializer.version>1.0.1</jackson-serializer.version>

        <!-- SonarQube: allow underscores in the method names of Spring Data JPA repositories -->
        <sonar.jacoco.reportPaths>${project.build.directory}/jacoco.exec</sonar.jacoco.reportPaths>
        <sonar.projectName>chvote-receiver</sonar.projectName>
        <sonar.issue.ignore.multicriteria>allowUnderscores</sonar.issue.ignore.multicriteria>
        <sonar.issue.ignore.multicriteria.allowUnderscores.ruleKey>squid:S00100
        </sonar.issue.ignore.multicriteria.allowUnderscores.ruleKey>
        <sonar.issue.ignore.multicriteria.allowUnderscores.resourceKey>**/repository/**/*Repository.java
        </sonar.issue.ignore.multicriteria.allowUnderscores.resourceKey>
    </properties>

    <dependencyManagement>
        <dependencies>

            <!-- ch.ge.ve group id -->
            <dependency>
                <groupId>ch.ge.ve</groupId>
                <artifactId>crypto-impl</artifactId>
                <version>${chvote-crypto.version}</version>
            </dependency>
            <dependency>
                <groupId>ch.ge.ve.interfaces</groupId>
                <artifactId>chvote-model-converter-api</artifactId>
                <version>${model-converter.version}</version>
            </dependency>
            <dependency>
                <groupId>ch.ge.ve</groupId>
                <artifactId>pact-b2b-client</artifactId>
                <version>${pact-b2b-client.version}</version>
            </dependency>
            <dependency>
                <groupId>ch.ge.ve</groupId>
                <artifactId>jackson-serializer</artifactId>
                <version>${jackson-serializer.version}</version>
            </dependency>
            <dependency>
                <groupId>ch.ge.ve</groupId>
                <artifactId>protocol-client</artifactId>
                <version>${chvote-protocol-core.version}</version>
            </dependency>
            <dependency>
                <groupId>ch.ge.ve</groupId>
                <artifactId>protocol-support</artifactId>
                <version>${chvote-protocol-core.version}</version>
            </dependency>


            <!-- com.google.guava group id -->
            <dependency>
                <groupId>com.google.guava</groupId>
                <artifactId>guava</artifactId>
                <version>${guava.version}</version>
            </dependency>

            <!-- commons-fileupload group id -->
            <dependency>
                <groupId>commons-fileupload</groupId>
                <artifactId>commons-fileupload</artifactId>
                <version>${commons-fileupload.version}</version>
            </dependency>

            <!-- com.fasterxml.jackson.core group id -->
            <dependency>
                <groupId>com.fasterxml.jackson.core</groupId>
                <artifactId>jackson-databind</artifactId>
                <version>${jackson.version}</version>
            </dependency>
            <dependency>
                <groupId>com.fasterxml.jackson.core</groupId>
                <artifactId>jackson-annotations</artifactId>
                <version>${jackson.version}</version>
            </dependency>

            <!-- com.h2database group id -->
            <dependency>
                <groupId>com.h2database</groupId>
                <artifactId>h2</artifactId>
                <version>${h2.version}</version>
                <scope>runtime</scope>
            </dependency>

            <!-- cglib group id -->
            <dependency>
                <groupId>cglib</groupId>
                <artifactId>cglib-nodep</artifactId>
                <version>${cglib-nodep.version}</version>
                <scope>test</scope>
            </dependency>

            <!-- org.bouncycastle group id -->
            <dependency>
                <groupId>org.bouncycastle</groupId>
                <artifactId>bcprov-jdk15on</artifactId>
                <version>${bouncycastle.version}</version>
            </dependency>

            <!-- org.json group id -->
            <dependency>
                <groupId>org.json</groupId>
                <artifactId>json</artifactId>
                <version>${json.version}</version>
            </dependency>

            <!-- org.mockito group id -->
            <dependency>
                <groupId>org.mockito</groupId>
                <artifactId>mockito-core</artifactId>
                <version>${mockito.version}</version>
                <scope>test</scope>
            </dependency>

            <!-- org.spockframework group id -->
            <dependency>
                <groupId>org.spockframework</groupId>
                <artifactId>spock-core</artifactId>
                <version>${spock.version}</version>
                <scope>test</scope>
            </dependency>

            <dependency>
                <groupId>org.spockframework</groupId>
                <artifactId>spock-spring</artifactId>
                <version>${spock.version}</version>
                <scope>test</scope>
            </dependency>

            <!-- io.swagger group id -->
            <dependency>
                <groupId>io.swagger.core.v3</groupId>
                <artifactId>swagger-annotations</artifactId>
                <version>${swagger-annotations.version}</version>
            </dependency>

            <!-- org.objenesis group id -->
            <dependency>
                <groupId>org.objenesis</groupId>
                <artifactId>objenesis</artifactId>
                <version>${objenesis.version}</version>
                <scope>test</scope>
            </dependency>

        </dependencies>
    </dependencyManagement>


    <build>
        <pluginManagement>
            <plugins>
                <plugin>
                    <groupId>com.github.kongchen</groupId>
                    <artifactId>swagger-maven-plugin</artifactId>
                    <version>${swagger-maven-plugin-version}</version>
                </plugin>

                <plugin>
                    <artifactId>maven-compiler-plugin</artifactId>
                    <dependencies>
                        <dependency>
                            <groupId>org.ow2.asm</groupId>
                            <artifactId>asm</artifactId>
                            <version>${asm.version}</version>
                        </dependency>
                    </dependencies>
                    <configuration>
                        <release>11</release>
                    </configuration>
                </plugin>

                <plugin>
                    <groupId>org.codehaus.gmavenplus</groupId>
                    <artifactId>gmavenplus-plugin</artifactId>
                    <version>1.6.2</version>
                    <configuration>
                        <targetBytecode>1.8</targetBytecode>
                    </configuration>
                    <executions>
                        <execution>
                            <goals>
                                <goal>addTestSources</goal>
                                <goal>compileTests</goal>
                                <goal>removeTestStubs</goal>
                            </goals>
                        </execution>
                    </executions>
                </plugin>
                <plugin>
                    <groupId>pl.project13.maven</groupId>
                    <artifactId>git-commit-id-plugin</artifactId>
                    <configuration>
                        <dotGitDirectory>${project.basedir}/.git</dotGitDirectory>
                        <commitIdGenerationMode>flat</commitIdGenerationMode>
                        <abbrevLength>8</abbrevLength>
                        <gitDescribe>
                            <skip>true</skip>
                        </gitDescribe>
                    </configuration>
                </plugin>

                <plugin>
                    <groupId>org.jacoco</groupId>
                    <artifactId>jacoco-maven-plugin</artifactId>
                    <version>${jacoco-maven-plugin.version}</version>
                </plugin>

            </plugins>
        </pluginManagement>

        <plugins>
            <plugin>
                <groupId>org.codehaus.gmavenplus</groupId>
                <artifactId>gmavenplus-plugin</artifactId>
            </plugin>

            <plugin>
                <artifactId>maven-surefire-plugin</artifactId>
                <dependencies>
                    <dependency>
                        <groupId>org.ow2.asm</groupId>
                        <artifactId>asm</artifactId>
                        <version>${asm.version}</version>
                    </dependency>
                </dependencies>
            </plugin>

            <!-- Code coverage -->
            <plugin>
                <groupId>org.jacoco</groupId>
                <artifactId>jacoco-maven-plugin</artifactId>
                <configuration>
                    <append>true</append>
                    <destFile>${sonar.jacoco.reportPaths}</destFile>
                </configuration>
                <executions>
                    <execution>
                        <id>agent-for-unit-tests</id>
                        <goals>
                            <goal>prepare-agent</goal>
                        </goals>
                    </execution>
                    <execution>
                        <id>jacoco-site</id>
                        <phase>verify</phase>
                        <goals>
                            <goal>report</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-source-plugin</artifactId>
                <executions>
                    <execution>
                        <id>attach-sources</id>
                        <goals>
                            <goal>jar</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-javadoc-plugin</artifactId>
                <configuration>
                    <doclint>all,-reference</doclint>
                    <!-- Silence error javax.interceptor.InterceptorBinding not found -->
                    <!--    see https://stackoverflow.com/questions/27808734 -->
                    <additionalDependencies>
                        <additionalDependency>
                            <groupId>javax.interceptor</groupId>
                            <artifactId>javax.interceptor-api</artifactId>
                            <version>${javax-interceptor-api.version}</version>
                        </additionalDependency>
                    </additionalDependencies>
                </configuration>
                <executions>
                    <execution>
                        <id>attach-javadocs</id>
                        <phase>deploy</phase>
                        <goals>
                            <goal>jar</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>

        </plugins>
    </build>

</project>

