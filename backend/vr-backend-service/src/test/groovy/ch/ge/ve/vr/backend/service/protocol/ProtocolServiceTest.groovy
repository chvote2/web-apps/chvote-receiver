/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

package ch.ge.ve.vr.backend.service.protocol

import ch.ge.ve.protocol.client.ProtocolClient
import ch.ge.ve.protocol.client.exception.ConfirmationFailedException
import ch.ge.ve.protocol.core.model.FinalizationCodePart
import ch.ge.ve.protocol.model.BigIntPair
import ch.ge.ve.protocol.model.Confirmation
import ch.ge.ve.protocol.model.NonInteractiveZkp
import ch.ge.ve.vr.backend.repository.entity.VoterHolder
import ch.ge.ve.vr.backend.service.voter.VoterService
import spock.lang.Specification

class ProtocolServiceTest extends Specification {

  public static final Confirmation CONFIRMATION = new Confirmation(BigInteger.ONE, new NonInteractiveZkp([], []))
  public static
  final FinalizationCodePart FIN_CODE_PART = new FinalizationCodePart("test".bytes, new BigIntPair(BigInteger.ONE, BigInteger.TWO))
  def protocolClientFactory = Mock(ProtocolClientFactoryImpl)
  def voterService = Mock(VoterService)
  def protocolService = new ProtocolService(protocolClientFactory, voterService)
  def client = Mock(ProtocolClient)

  void setup() {
    protocolClientFactory.client(_ as String) >> client
  }


  def "SubmitConfirmationCode should succeed if the confirmation is correct"() {
    given:
    VoterHolder voter = new VoterHolder()
    voterService.getVoter("pid", 1) >> voter
    client.submitConfirmation(1, CONFIRMATION) >> [FIN_CODE_PART]

    when:
    def result = protocolService.submitConfirmationCode(CONFIRMATION, "pid", 1)

    then:
    result == [FIN_CODE_PART]
  }


  def "SubmitConfirmationCode should failed if the confirmation failed"() {
    given:
    VoterHolder voter = new VoterHolder()

    voterService.getVoter("pid", 1) >> voter


    client.submitConfirmation(1, CONFIRMATION) >> { throw new ConfirmationFailedException() }

    when:
    protocolService.submitConfirmationCode(CONFIRMATION, "pid", 1)

    then:
    1 * voterService.markVoterConfirmationFailed(voter)
  }


  def "submitBallot should call markVerificationCodeRevealed once"() {
    given:

    client.publishBallot(1, null) >> Collections.emptyList()

    when:
    protocolService.submitBallot(null, "pid", 1)

    then:
    1 * voterService.markVerificationCodeRevealed("pid", 1)
  }

}
