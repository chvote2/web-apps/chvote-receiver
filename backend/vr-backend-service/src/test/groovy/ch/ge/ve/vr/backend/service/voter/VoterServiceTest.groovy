/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

package ch.ge.ve.vr.backend.service.voter

import ch.ge.ve.vr.backend.fixtures.DoiIds
import ch.ge.ve.vr.backend.repository.VoterRepository
import ch.ge.ve.vr.backend.repository.entity.VoterHolder
import ch.ge.ve.vr.backend.service.exception.BirthDateCheckException
import ch.ge.ve.vr.backend.service.exception.ConfirmationCheckException
import ch.ge.ve.vr.backend.service.exception.UnknownVoterException
import ch.ge.ve.vr.backend.service.exception.VerificationCodeRevealedException
import ch.ge.ve.vr.backend.service.operation.BirthDateScopeMapper
import spock.lang.Specification

class VoterServiceTest extends Specification {

  def static HASH_SEPARATOR = ";"
  def static HASH_DATEOFBIRTH_PATTERN = /^([a-fA-F0-9]+)/ + HASH_SEPARATOR + /([0-9]+)/ + HASH_SEPARATOR + /([a-fA-F0-9]+)$/

  VoterRepository voterRepository = Mock(VoterRepository)
  VoterService voterService = new VoterService(voterRepository)

  def "GetVoter should throw UnknownVoterException if there is no such voter"() {
    given:
    voterRepository.findByProtocolInstanceIdAndVoterId("pid", 1) >>
            Optional.empty()

    when:
    voterService.getVoter("pid", 1)

    then:
    def exception = thrown(UnknownVoterException)
    exception.messageKey == "unknown-voter"
  }

  def "GetVoter"() {
    given:
    VoterHolder voterHolder = new VoterHolder()
    voterHolder.setId(1)
    voterRepository.findByProtocolInstanceIdAndVoterId("pid", 1) >>
            Optional.of(voterHolder)

    when:
    def result = voterService.getVoter("pid", 1)

    then:
    result.id == 1
  }

  def "IsVoterConfirmationAllowed should not thrown an exception when threshold is not reached"() {
    given:
    VoterHolder voter = new VoterHolder()
    voter.setId(1)
    voter.setConfirmationCheckFailed(faillure)

    when:
    voterService.checkVoterConfirmationAllowed(voter)

    then:
    notThrown(ConfirmationCheckException)

    where:
    _ | faillure
    _ | 0
    _ | 3
  }

  def "IsVoterConfirmationAllowed should thrown an exception when threshold is reached"() {
    given:
    VoterHolder voter = new VoterHolder()
    voter.setId(1)
    voter.setConfirmationCheckFailed(5)

    when:
    voterService.checkVoterConfirmationAllowed(voter)

    then:
    thrown(ConfirmationCheckException)
  }

  def "markVoterConfirmationFailed"() {
    given:
    VoterHolder voter = new VoterHolder()
    voter.setId(1)
    voter.setConfirmationCheckFailed(faillure)

    when:
    voterService.markVoterConfirmationFailed(voter)

    then:
    def ex = thrown(ConfirmationCheckException)
    ex.messageKey == messageKey
    ex.parameters == parameters as String[]
    voter.getConfirmationCheckFailed() == faillure + 1
    1 * voterRepository.save(voter)

    where:
    faillure | messageKey                           | parameters
    0        | "wrong-confirmation-code-submitted"  | [4, true]
    3        | "wrong-confirmation-code-submitted"  | [1, true]
    4        | "max-number-of-confirmation-reached" | [5, false]
  }

  def "getVoter should failed if user is unknown"() {
    given:
    voterRepository.findByProtocolInstanceIdAndVoterId("pid", 1) >> Optional.empty()

    when:
    voterService.getVoter("pid", 1)

    then:
    def exception = thrown(UnknownVoterException)
    exception.messageKey == "unknown-voter"
  }

  def "recovery from the hash date of birthday should return correct salt and hash"() {

    given:
    def salt = "abcdef123456789"
    def iterations = "12"
    def hash = "987654321fedcab"
    def dateOfBirthHashed = salt + HASH_SEPARATOR + iterations + HASH_SEPARATOR + hash

    when:
    def extractedSalt = voterService.getSalt(dateOfBirthHashed)
    def extractedHash = voterService.getHash(dateOfBirthHashed)

    then:
    extractedSalt == salt
    extractedHash == hash
  }

  def "recovery from an incorrect hash date of birthday should return an empty string"() {

    when:
    def salt = voterService.getSalt(dateOfBirthHashedIncorrect)
    def hash = voterService.getHash(dateOfBirthHashedIncorrect)

    then:
    salt == ""
    hash == ""

    where:
    _ | dateOfBirthHashedIncorrect
    _ | null
    _ | "abcedf" + HASH_SEPARATOR + "noHexa"
    _ | "noHexa" + HASH_SEPARATOR + "aaaaaaaaaaaaaaaaaaaaaaa"
    _ | "aa" + "anotherSeparator" + "aaaaaaaaaaaaaaaaaaaaaaa"
  }


  def "checkBirthDate should work if check date is provided"() {
    given:
    def protocolId = "test"
    def voter = createVoter(year, month, day, DoiIds.DOI_FED, DoiIds.DOI_GE)
    voter.setBirthDayCheckFailed(0)
    voterRepository.findByProtocolInstanceIdAndVoterId(protocolId, 1) >>
            Optional.of(voter)

    when:
    voterService.checkBirthDate(protocolId, 1, year, month, day)

    then:
    notThrown(BirthDateCheckException)

    where:
    day  | month | year
    1    | 1     | 2010
    null | 1     | 2011
    null | null  | 2012
  }

  def "checkBirthDate should thrown an exception if voter has already done too much attempt"() {
    given:
    def protocolId = "test"
    def voter = createVoter(2010, 10, 10, DoiIds.DOI_FED, DoiIds.DOI_GE)
    voter.setBirthDayCheckFailed(10)
    voterRepository.findByProtocolInstanceIdAndVoterId(protocolId, 1) >>
            Optional.of(voter)

    when:
    voterService.checkBirthDate(protocolId, 1, 2010, 10, 10)

    then:
    def ex = thrown(BirthDateCheckException)
    ex.messageKey == "max-number-of-birth-date-submitted"
  }

  def "checkBirthDate should thrown an exception if voter provide wrong birth date information"() {
    given:
    def protocolId = "test"
    def voter = createVoter(2010, 10, 11, DoiIds.DOI_FED, DoiIds.DOI_GE)
    voter.setBirthDayCheckFailed(0)
    voterRepository.findByProtocolInstanceIdAndVoterId(protocolId, 1) >>
            Optional.of(voter)

    when:
    voterService.checkBirthDate(protocolId, 1, 2010, 10, 10)

    then:
    def ex = thrown(BirthDateCheckException)
    ex.messageKey == "wrong-birth-date-submitted"
  }


  def "checkBirthDate should thrown an exception if voter provide wrong birth date information for the last time"() {
    given:
    def protocolId = "test"
    def voter = createVoter(2010, 10, 11, DoiIds.DOI_FED, DoiIds.DOI_GE)
    voter.setBirthDayCheckFailed(4)
    voterRepository.findByProtocolInstanceIdAndVoterId(protocolId, 1) >>
            Optional.of(voter)

    when:
    voterService.checkBirthDate(protocolId, 1, 2010, 10, 10)

    then:
    def ex = thrown(BirthDateCheckException)
    ex.messageKey == "max-number-of-birth-date-submitted"
  }

  def "checkVoterConfirmationAllowed should throw an exception if voter has too much confirmation already failed"() {
    given:
    VoterHolder voter = new VoterHolder()
    voter.setConfirmationCheckFailed(5)

    when:
    voterService.checkVoterConfirmationAllowed(voter)

    then:
    def exception = thrown(ConfirmationCheckException)
    exception.messageKey == "max-number-of-confirmation-reached"
  }


  def "checkVoterConfirmationAllowed should not throw exception if there is few confirmation failure"() {
    given:
    VoterHolder voter = new VoterHolder()
    voter.setConfirmationCheckFailed(3)

    when:
    voterService.checkVoterConfirmationAllowed(voter)

    then:
    notThrown(ConfirmationCheckException)
  }


  def "markVoterConfirmationFailed should failed if the confirmation failed"() {
    given:
    VoterHolder voter = new VoterHolder()
    voter.setConfirmationCheckFailed(3)

    when:
    voterService.markVoterConfirmationFailed(voter)

    then:
    def exception = thrown(ConfirmationCheckException)
    exception.messageKey == "wrong-confirmation-code-submitted"
    1 * voterRepository.save(voter)
    voter.confirmationCheckFailed == 4
  }

  def "markVoterConfirmationFailed should failed if the confirmation failed and it is the last chance"() {
    given:
    VoterHolder voter = new VoterHolder()
    voter.setConfirmationCheckFailed(4)

    when:
    voterService.markVoterConfirmationFailed(voter)

    then:
    def exception = thrown(ConfirmationCheckException)
    exception.messageKey == "max-number-of-confirmation-reached"
    1 * voterRepository.save(voter)
    voter.confirmationCheckFailed == 5
  }


  def "the hash of the date of birth should return correct values"() {
    given:
    def dateOfBirthDay = 31
    def dateOfBirthMonth = 12
    def dateOfBirthYear = 1919
    def regex = HASH_DATEOFBIRTH_PATTERN

    when:
    def hashDateOfBirth = voterService.hashDateOfBirth(dateOfBirthDay, dateOfBirthMonth, dateOfBirthYear)
    def match = hashDateOfBirth =~ regex

    then:
    hashDateOfBirth != null
    hashDateOfBirth ==~ regex
    hashDateOfBirth.length() == 167
    match[0][1].toString().length() == 32
    Integer.valueOf(match[0][2]) >= 10000
    Integer.valueOf(match[0][2]) <= 11000
    match[0][3].toString().length() == 128
  }

  def "the hash of multiple dates of birth should return correct values"() {
    given:
    def regex = HASH_DATEOFBIRTH_PATTERN //Salt in hexa + SEPARATOR + iterations + SEPARATOR + Hash in hexa

    when:
    def hashDateOfBirth = voterService.hashDateOfBirth(dateOfBirthDay, dateOfBirthMonth, dateOfBirthYear)
    def match = hashDateOfBirth =~ regex

    then:
    hashDateOfBirth != null
    hashDateOfBirth ==~ regex
    hashDateOfBirth.length() == 167
    match[0][1].toString().length() == 32
    Integer.valueOf(match[0][2]) >= 10000
    Integer.valueOf(match[0][2]) <= 11000
    match[0][3].toString().length() == 128

    where:
    dateOfBirthDay | dateOfBirthMonth | dateOfBirthYear
    31             | 12               | 1919
    0              | 12               | 1919
    -1             | 12               | 1919
    null           | 12               | 1919
    null           | null             | null
    654654         | null             | null
    654654         | 56464            | null
    654654         | 56464            | 88888
  }

  def "isValidHash should true if dateOfBirthHashed match pattern's hash"() {
    given:

    when:
    def isValidHash = voterService.isValidHash(dateOfBirthHashed)

    then:
    isValidHash == expectedResult

    where:
    dateOfBirthHashed                                                             || expectedResult
    "aaa" + HASH_SEPARATOR + "10000" + HASH_SEPARATOR + "bbbb"                    || true
    "aaa" + HASH_SEPARATOR + "10000" + "another_HASH_SEPARATOR" + "bbbb"          || false
    "aaa" + HASH_SEPARATOR + "not an integer" + "another_HASH_SEPARATOR" + "bbbb" || false
    "NOT_Hexa" + HASH_SEPARATOR + "10000" + HASH_SEPARATOR + "bbbb"               || false
    "aaa" + HASH_SEPARATOR + "10000" + HASH_SEPARATOR + "NOT_Hexa"                || false
    null + HASH_SEPARATOR + "10000" + HASH_SEPARATOR + null                       || false
    null                                                                          || false
    ""                                                                            || false
  }

  def "dateAndHashAreSame should true if hash date is egal to dateOfBirthHashed"() {
    given:
    def day = 1
    def month = 1
    def year = 2000

    when:
    def dateOfBirthHashed = voterService.hashDateOfBirth(day, month, year)
    def isSameHash = voterService.dateAndHashAreSame(day, month, year, dateOfBirthHashed)

    then:
    isSameHash == true
  }

  def "toHex should be reverse by toString"() {
    given:
    def inputString = "a test string"

    when:
    def hex = voterService.toHexa(inputString.getBytes())
    def reverseHex = voterService.toByte(hex)

    then:
    reverseHex != null
    inputString != null
    inputString == new String(reverseHex)
  }


  def "markVerificationCodeRevealed should succeed"() {
    given:
    VoterHolder voterHolder = new VoterHolder()
    voterHolder.setId(1)
    voterHolder.setVerificationCodeRevealed(true)
    voterRepository.findByProtocolInstanceIdAndVoterId("pid", 1) >>
            Optional.of(voterHolder)

    when:
    voterService.markVerificationCodeRevealed("pid",1)

    then:
    1 * voterRepository.save(voterHolder)
    voterHolder.verificationCodeRevealed
  }

  def "checkVerificationCodeNotRevealed should throw an exception if the confirmation revealed"() {
    given:
    VoterHolder voterHolder = new VoterHolder()
    voterHolder.setId(1)
    voterHolder.setVerificationCodeRevealed(true)
    voterRepository.findByProtocolInstanceIdAndVoterId("pid", 1) >>
            Optional.of(voterHolder)

    when:
    voterService.checkVerificationCodeNotRevealed(voterHolder)

    then:
    def exception = thrown(VerificationCodeRevealedException)
    exception.messageKey == "verification-code-revealed"
  }

  VoterHolder createVoter(int year, Integer month, Integer day, String... doiIds) {
    def voter = new VoterHolder()
    voter.domainOfInfluences = doiIds as Set
    voter.voterId = 1
    voter.voterBirthDateScope = BirthDateScopeMapper.mapToScope(day, month)
    voter.voterBirthHashed = voterService.hashDateOfBirth(day, month, year)
    voter
  }

}
