/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

package ch.ge.ve.vr.backend.service

import static ch.ge.ve.vr.backend.service.model.BirthDateScope.DAY
import static ch.ge.ve.vr.backend.service.model.BirthDateScope.MONTH
import static ch.ge.ve.vr.backend.service.model.BirthDateScope.YEAR

import ch.ge.ve.vr.backend.fixtures.DoiIds
import ch.ge.ve.vr.backend.fixtures.OperationInstance
import ch.ge.ve.vr.backend.fixtures.TestCases
import ch.ge.ve.vr.backend.repository.ElectionBallotRepository
import ch.ge.ve.vr.backend.repository.FinalizationLogRepository
import ch.ge.ve.vr.backend.repository.VotationBallotRepository
import ch.ge.ve.vr.backend.repository.entity.ElectionHolder
import ch.ge.ve.vr.backend.repository.entity.FinalizationLog
import ch.ge.ve.vr.backend.repository.entity.OperationHolder
import ch.ge.ve.vr.backend.repository.entity.VotationHolder
import ch.ge.ve.vr.backend.repository.entity.VoterHolder
import ch.ge.ve.vr.backend.service.config.VoteReceiverConfigurationProperties
import ch.ge.ve.vr.backend.service.exception.BirthDateCheckException
import ch.ge.ve.vr.backend.service.operation.BirthDateScopeMapper
import ch.ge.ve.vr.backend.service.operation.OperationService
import ch.ge.ve.vr.backend.service.voter.VoterService
import java.time.LocalDateTime
import spock.lang.Specification

class IdentificationServiceTest extends Specification {

  ElectionBallotRepository electionBallotRepository = Mock(ElectionBallotRepository)
  VotationBallotRepository votationBallotRepository = Mock(VotationBallotRepository)
  FinalizationLogRepository finalizationLogRepository = Mock(FinalizationLogRepository)

  VoteReceiverConfigurationProperties config = new VoteReceiverConfigurationProperties()
  OperationService operationService = Mock(OperationService)
  VoterService voterService = Mock(VoterService)
  VrumService vrumService = Mock(VrumService)

  IdentificationService identificationService = new IdentificationService(
          electionBallotRepository,
          votationBallotRepository,
          finalizationLogRepository,
          TestHelpers.jsonDocumentEntityService(),
          config,
          operationService,
          voterService,
          vrumService)


  void setup() {
    config.modelVersion = 2
  }

  def "identification should retrieve votation and election if user has appropriate DOI"() {
    given:
    def protocolId = "test"
    def operationInstance = TestCases.createSimpleOperation()
    initMockWithTestCase(protocolId, operationInstance)
    voterService.getVoter(protocolId, 1) >> createVoter(year, month, day, DoiIds.DOI_FED, DoiIds.DOI_GE)

    when:
    def data = identificationService.identification(true, protocolId, 1)

    then:

    data.votations.size() == 2
    data.elections.size() == 0
    data.birthDateScope == birthDateScope
    1 * vrumService.assertCanIdentify(protocolId, 1)
    1 * operationService.checkOperationAccess(true, protocolId)
    1 * voterService.checkVoterAuthenticationAllowed(_ as VoterHolder)
    1 * voterService.checkVoterConfirmationAllowed(_ as VoterHolder)


    where:
    year | month | day  || birthDateScope
    2010 | 1     | 1    || DAY
    2010 | 1     | null || MONTH
    2010 | null  | null || YEAR

  }


  def "identification should retrieve no votation or election if user has other DOI"() {
    given:
    def protocolId = "test"
    def operationInstance = TestCases.createSimpleOperation()
    initMockWithTestCase(protocolId, operationInstance)
    voterService.getVoter(protocolId, 1) >>
            createVoter(2010, 1, 1, DoiIds.DOI_BE)

    when:
    def data = identificationService.identification(true, protocolId, 1)

    then:
    data.votations.size() == 0
    data.elections.size() == 0
    1 * vrumService.assertCanIdentify(protocolId, 1)
    1 * voterService.checkVoterAuthenticationAllowed(_ as VoterHolder)
    1 * voterService.checkVoterConfirmationAllowed(_ as VoterHolder)
    1 * voterService.checkVerificationCodeNotRevealed(_ as VoterHolder)
  }

  def "fetchFinalizationLog should return finalization log if user has already voted"() {
    given:
    def protocolId = "test"
    def voterId = 1

    def finalizationLog = new FinalizationLog()
    finalizationLog.setId(1)
    finalizationLog.setVoterId(voterId)
    finalizationLog.setCode("123456")
    finalizationLog.setDate(LocalDateTime.now())

    when:
    def finalizationLogData = identificationService.fetchFinalizationLog(true, protocolId, voterId)

    then:
    finalizationLogData.code == finalizationLog.code
    finalizationLogData.date == finalizationLog.date
    1 * operationService.checkOperationAccess(true, protocolId) >> 1
    1 * finalizationLogRepository.findByOperationIdAndVoterId(1, voterId) >> finalizationLog
  }

  def "fetchFinalizationLog should return null if user has not voted"() {
    given:
    def protocolId = "test"
    def voterId = 1

    when:
    def finalizationLogData = identificationService.fetchFinalizationLog(true, protocolId, voterId)

    then:
    finalizationLogData == null
    1 * operationService.checkOperationAccess(true, protocolId) >> 1
    1 * finalizationLogRepository.findByOperationIdAndVoterId(1, voterId) >> null
  }


  def "checkBirthDate should work if check date is provided"() {
    given:
    def protocolId = "test"
    def voter = createVoter(year, month, day, DoiIds.DOI_FED, DoiIds.DOI_GE)
    voter.setBirthDayCheckFailed(0)

    when:
    identificationService.checkBirthDate(true, protocolId, 1, year, month, day)

    then:
    notThrown(BirthDateCheckException)

    where:
    day  | month | year
    1    | 1     | 2010
    null | 1     | 2011
    null | null  | 2012
  }

  def "logFinalization should log a finalization vote in the database"() {
    given:
    def protocolInstanceId = '12345-67890'
    def voterId = 1

    def operationHolder = new OperationHolder()
    operationHolder.setProtocolInstanceId(protocolInstanceId)

    def logDate = LocalDateTime.now()
    def finalizationLog = new FinalizationLog()
    finalizationLog.setOperation(operationHolder)
    finalizationLog.setVoterId(voterId)
    finalizationLog.setCode('123456')
    finalizationLog.setDate(logDate)

    when:
    operationService.getOperation(protocolInstanceId) >> operationHolder
    def finalizationLogData = identificationService.logFinalization(protocolInstanceId, voterId, '123456')

    then:
    finalizationLogData.code == '123456'
    finalizationLogData.date == logDate
    1 * finalizationLogRepository.save(finalizationLog) >> finalizationLog
  }

  def initMockWithTestCase(String protocolId, OperationInstance operationInstance) {
    def elections = operationInstance.elections.collect {
      def electionBallot = new ElectionHolder()
      electionBallot.data = TestHelpers.objectMapper.writeValueAsString(it)
      electionBallot
    }
    electionBallotRepository.findByOperation_ProtocolInstanceIdAndOperation_ModelVersion(protocolId, config.modelVersion) >> elections

    def votations = operationInstance.votations.collect {
      def votationBallot = new VotationHolder()
      votationBallot.data = TestHelpers.objectMapper.writeValueAsString(it)
      votationBallot
    }
    votationBallotRepository.findByOperation_ProtocolInstanceIdAndOperation_ModelVersion(protocolId, config.modelVersion) >> votations
  }

  VoterHolder createVoter(int year, Integer month, Integer day, String... doiIds) {
    VoterHolder voter = new VoterHolder()
    voter.domainOfInfluences = doiIds as Set
    voter.voterId = 1
    voter.voterBirthDateScope = BirthDateScopeMapper.mapToScope(day, month)
    voter.voterBirthHashed = voterService.hashDateOfBirth(day, month, year)
    return voter
  }

}
