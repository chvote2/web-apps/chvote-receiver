/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

package ch.ge.ve.vr.backend.service

import ch.ge.ve.vr.backend.service.config.VrumConfiguration
import org.apache.http.client.fluent.Executor
import spock.lang.Specification

class VrumServiceTest extends Specification {
  VrumConfiguration configuration = Mock(VrumConfiguration)
  Executor executor = Mock(Executor)

  VrumService vrumService = new VrumService(configuration, executor)


  def "#AssertCanIdentify should throw an MissingMethodException if voterId is not of type int"() {
    given:
    def protocolInstanceId = "protocolInstanceId"
    def voterId = "1"

    when:
    vrumService.assertCanIdentify(protocolInstanceId, voterId)

    then:
    thrown(MissingMethodException)
  }

  def "#AssertCanIdentify should throw an MissingMethodException the two params are inverted"() {
    given:
    def protocolInstanceId = "protocolInstanceId"
    def voterId = 1

    when:
    vrumService.assertCanIdentify(voterId, protocolInstanceId)

    then:
    thrown(MissingMethodException)
  }

}
