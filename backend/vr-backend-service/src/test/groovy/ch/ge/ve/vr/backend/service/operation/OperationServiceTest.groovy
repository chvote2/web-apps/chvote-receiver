/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

package ch.ge.ve.vr.backend.service.operation

import ch.ge.ve.chvote.pact.b2b.client.model.Lang
import ch.ge.ve.chvote.pact.b2b.client.model.OperationType
import ch.ge.ve.chvote.pact.b2b.client.model.VotingPeriod
import ch.ge.ve.vr.backend.fixtures.OperationInstance
import ch.ge.ve.vr.backend.fixtures.TestCases
import ch.ge.ve.vr.backend.repository.OperationRepository
import ch.ge.ve.vr.backend.repository.entity.OperationHolder
import ch.ge.ve.vr.backend.service.TestHelpers
import ch.ge.ve.vr.backend.service.config.VoteReceiverConfigurationProperties
import ch.ge.ve.vr.backend.service.exception.AccessSecurityException
import ch.ge.ve.vr.backend.service.model.OperationStatus
import java.time.Clock
import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneId
import javax.persistence.EntityNotFoundException
import spock.lang.Specification

class OperationServiceTest extends Specification {

  OperationRepository operationRepository = Mock(OperationRepository)
  VoteReceiverConfigurationProperties config = Mock(VoteReceiverConfigurationProperties)

  def fixedClock = Clock.fixed(Instant.EPOCH, ZoneId.systemDefault())
  def operationService = new OperationService(operationRepository, fixedClock,
          config, TestHelpers.jsonDocumentEntityService())

  def pastInScopeDate = LocalDateTime.now(fixedClock).withNano(0).minusDays(20)
  def tomorrowDate = LocalDateTime.now(fixedClock).withNano(0).plusDays(1)

  VotingPeriod pastInScopeVotingPeriod = new VotingPeriod(pastInScopeDate, pastInScopeDate.plusDays(10), 9)
  VotingPeriod inProgressVotingPeriod = new VotingPeriod(pastInScopeDate, pastInScopeDate.plusDays(30), 9)
  VotingPeriod futureVotingPeriod = new VotingPeriod(tomorrowDate, tomorrowDate.plusDays(30), 9)

  void setup() {
    config.modelVersion >> 2
  }

  def "getOperation should failed when requesting absent operation"() {
    given:
    operationRepository.findByProtocolInstanceIdAndModelVersion("test", 2) >> Optional.empty()

    when:
    operationService.getOperation(true, "test")

    then:
    def exception = thrown(EntityNotFoundException)
    exception.message == "cannot find operation for protocol instance ID test"
  }


  def "getOperation should failed retrieving REAL operation in test mode"() {
    given:
    def operationInstance = TestCases.createSimpleOperation(OperationType.REAL, inProgressVotingPeriod, inProgressVotingPeriod.closingDate.plusDays(1))
    def operation = new OperationHolder()
    operation.setType(OperationType.REAL)
    operation.setData(TestHelpers.objectMapper.writeValueAsString(operationInstance.operation))
    operationRepository.findByProtocolInstanceIdAndModelVersion("test", 2) >> Optional.of(operation)

    when:
    operationService.getOperation(true, "test")

    then:
    def exception = thrown(AccessSecurityException)
    exception.message == "Try to access operation typed REAL in private mode"
  }

  def "getOperation should success retrieving TEST operation in test mode"() {
    given:
    def operationInstance = TestCases.createSimpleOperation(OperationType.TEST, inProgressVotingPeriod, inProgressVotingPeriod.closingDate.plusDays(1))
    def operation = new OperationHolder()
    operation.setType(OperationType.TEST)
    operation.setData(TestHelpers.objectMapper.writeValueAsString(operationInstance.operation))
    operationRepository.findByProtocolInstanceIdAndModelVersion("test", 2) >> Optional.of(operation)

    when:
    def ope = operationService.getOperation(true, "test")

    then:
    ope.votingCardLabel.startsWith('Opération virtuelle')
  }


  def "findOperations should retrieve a summary of the operation"() {
    given:
    def operationInstance = TestCases.createSimpleOperation(OperationType.TEST, inProgressVotingPeriod, inProgressVotingPeriod.closingDate.plusDays(1))
    def operation = setupOperation(operationInstance)
    operationRepository.findAllForTestMode(2, "GE", _ as LocalDateTime) >> [operation]

    when:
    def map = operationService.findOperations("GE", true)
    def testList = map.get(OperationStatus.IN_TEST)
    def result = testList.get(0)

    then:
    result.protocolInstanceId == operationInstance.operation.protocolInstanceId
    result.label == operationInstance.operation.votingCardLabel
    result.defaultLang == Lang.FR
    result.votingPeriod == null
    result.date == inProgressVotingPeriod.closingDate.plusDays(1)
  }

  def "findOperations should retrieve summaries of operations correctly mapped"() {
    given:
    def pastTestOperation1 = TestCases.createSimpleOperation(OperationType.TEST, pastInScopeVotingPeriod, pastInScopeVotingPeriod.closingDate.plusDays(1))
    def pastTestOperation2 = TestCases.createSimpleOperation(OperationType.TEST, pastInScopeVotingPeriod, pastInScopeVotingPeriod.closingDate.plusDays(1))
    def inProgressTestOperation = TestCases.createSimpleOperation(OperationType.TEST, inProgressVotingPeriod, inProgressVotingPeriod.closingDate.plusDays(1))
    def futureTestOperation = TestCases.createSimpleOperation(OperationType.TEST, futureVotingPeriod, futureVotingPeriod.closingDate.plusDays(1))

    def pastSimulationOperation = TestCases.createSimpleOperation(OperationType.SIMULATION, pastInScopeVotingPeriod, pastInScopeVotingPeriod.closingDate.plusDays(1))
    def inProgressSimulationOperation1 = TestCases.createSimpleOperation(OperationType.SIMULATION, inProgressVotingPeriod, inProgressVotingPeriod.closingDate.plusDays(1))
    def inProgressSimulationOperation2 = TestCases.createSimpleOperation(OperationType.SIMULATION, inProgressVotingPeriod, inProgressVotingPeriod.closingDate.plusDays(1))
    def futureSimulationOperation1 = TestCases.createSimpleOperation(OperationType.SIMULATION, futureVotingPeriod, futureVotingPeriod.closingDate.plusDays(1))
    def futureSimulationOperation2 = TestCases.createSimpleOperation(OperationType.SIMULATION, futureVotingPeriod, futureVotingPeriod.closingDate.plusDays(1))
    def futureSimulationOperation3 = TestCases.createSimpleOperation(OperationType.SIMULATION, futureVotingPeriod, futureVotingPeriod.closingDate.plusDays(1))

    operationRepository.findAllForTestMode(2, "GE", _ as LocalDateTime) >> [
            setupOperation(pastTestOperation1),
            setupOperation(pastTestOperation2),
            setupOperation(inProgressTestOperation),
            setupOperation(futureTestOperation),
            setupOperation(pastSimulationOperation),
            setupOperation(inProgressSimulationOperation1),
            setupOperation(inProgressSimulationOperation2),
            setupOperation(futureSimulationOperation1),
            setupOperation(futureSimulationOperation2),
            setupOperation(futureSimulationOperation3)
    ]

    when:
    def map = operationService.findOperations("GE", true)
    System.out.println(map)
    def testList = map.get(OperationStatus.IN_TEST)
    def pastList = map.get(OperationStatus.PAST)
    def inProgressList = map.get(OperationStatus.IN_PROGRESS)
    def futureList = map.get(OperationStatus.FUTURE)

    then:
    testList.size() == 4
    pastList.size() == 1
    inProgressList.size() == 2
    futureList.size() == 3
  }

  OperationHolder setupOperation(OperationInstance operationInstance) {
    def operation = new OperationHolder()
    operation.setType(operationInstance.operation.type)
    operation.setProtocolInstanceId(operationInstance.operation.protocolInstanceId)
    operation.setLabel(operationInstance.operation.votingCardLabel)
    operation.setDefaultLang(operationInstance.operation.defaultLang)
    operation.setDate(operationInstance.operation.date)
    operation.setOpeningDate(operationInstance.operation.votingPeriod.openingDate)
    operation.setClosingDate(operationInstance.operation.votingPeriod.closingDate)
    operation.setGracePeriod(operationInstance.operation.votingPeriod.gracePeriod)
    operation.setData(TestHelpers.objectMapper.writeValueAsString(operationInstance.operation))
    return operation
  }
}