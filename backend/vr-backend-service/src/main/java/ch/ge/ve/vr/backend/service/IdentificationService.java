/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

package ch.ge.ve.vr.backend.service;

import ch.ge.ve.vr.backend.repository.ElectionBallotRepository;
import ch.ge.ve.vr.backend.repository.FinalizationLogRepository;
import ch.ge.ve.vr.backend.repository.VotationBallotRepository;
import ch.ge.ve.vr.backend.repository.data.Election;
import ch.ge.ve.vr.backend.repository.data.Votation;
import ch.ge.ve.vr.backend.repository.entity.Data;
import ch.ge.ve.vr.backend.repository.entity.FinalizationLog;
import ch.ge.ve.vr.backend.repository.entity.OperationHolder;
import ch.ge.ve.vr.backend.repository.entity.VoterHolder;
import ch.ge.ve.vr.backend.service.config.VoteReceiverConfigurationProperties;
import ch.ge.ve.vr.backend.service.exception.BirthDateCheckException;
import ch.ge.ve.vr.backend.service.model.FinalizationLogData;
import ch.ge.ve.vr.backend.service.model.VoterOperationData;
import ch.ge.ve.vr.backend.service.operation.OperationService;
import ch.ge.ve.vr.backend.service.voter.VoterService;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import javax.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service containing the logic to manage voters identification
 */
@Service
public class IdentificationService {

  private final ElectionBallotRepository            electionBallotRepository;
  private final VotationBallotRepository            votationBallotRepository;
  private final FinalizationLogRepository           finalizationLogRepository;
  private final JsonDocumentEntityService           jsonDocumentEntityService;
  private final VoteReceiverConfigurationProperties config;
  private final OperationService                    operationService;
  private final VoterService                        voterService;
  private final VrumService                         vrumService;

  @Autowired
  public IdentificationService(ElectionBallotRepository electionBallotRepository,
                               VotationBallotRepository votationBallotRepository,
                               FinalizationLogRepository finalizationLogRepository,
                               JsonDocumentEntityService jsonDocumentEntityService,
                               VoteReceiverConfigurationProperties config,
                               OperationService operationService,
                               VoterService voterService,
                               VrumService vrumService) {
    this.electionBallotRepository = electionBallotRepository;
    this.votationBallotRepository = votationBallotRepository;
    this.jsonDocumentEntityService = jsonDocumentEntityService;
    this.finalizationLogRepository = finalizationLogRepository;
    this.config = config;
    this.operationService = operationService;
    this.voterService = voterService;
    this.vrumService = vrumService;
  }

  /**
   * Perform base identification for a given voter ID. Check voter's access rights and
   * return the allowed ballots.
   *
   * @param forTest            true if this is a test operation
   * @param protocolInstanceId the protocol's instance ID
   * @param voterId            the voter's ID
   *
   * @return the operation data linked to this voter
   */

  @Transactional(readOnly = true)
  public VoterOperationData identification(boolean forTest, String protocolInstanceId, int voterId) {
    operationService.checkOperationAccess(forTest, protocolInstanceId);
    VoterHolder voter = voterService.getVoter(protocolInstanceId, voterId);

    vrumService.assertCanIdentify(protocolInstanceId, voterId);
    voterService.checkVoterAuthenticationAllowed(voter);
    voterService.checkVoterConfirmationAllowed(voter);
    voterService.checkVerificationCodeNotRevealed(voter);

    return new VoterOperationData(
        filterByDoi(votationBallotRepository.findByOperation_ProtocolInstanceIdAndOperation_ModelVersion(
            protocolInstanceId, config.getModelVersion()), Votation::getDomainOfInfluence,
                    voter.getDomainOfInfluences()),
        filterByDoi(electionBallotRepository.findByOperation_ProtocolInstanceIdAndOperation_ModelVersion(
            protocolInstanceId, config.getModelVersion()), Election::getDomainOfInfluence,
                    voter.getDomainOfInfluences()),
        ch.ge.ve.vr.backend.service.model.BirthDateScope.of(
            voter.getVoterBirthDateScope().equals(VoterHolder.BirthDateScope.YEAR_MONTH) ||
            voter.getVoterBirthDateScope().equals(VoterHolder.BirthDateScope.YEAR_MONTH_DAY),
            voter.getVoterBirthDateScope().equals(VoterHolder.BirthDateScope.YEAR_MONTH_DAY)
        )
    );
  }


  /**
   * Perform a check on birth date
   *
   * @param forTest            true if this is a test operation
   * @param protocolInstanceId the protocol's instance ID
   * @param voterId            the voter's ID
   * @param year               birth year
   * @param month              birth month (optional)
   * @param day                birth (optional)
   */
  @Transactional(noRollbackFor = BirthDateCheckException.class)
  public void checkBirthDate(boolean forTest, String protocolInstanceId, int voterId, Integer year, Integer month,
                             Integer day) {

    operationService.checkOperationAccess(forTest, protocolInstanceId);
    voterService.checkBirthDate(protocolInstanceId, voterId, year, month, day);

  }


  /**
   * Fetch the finalization log linked to the given protocol and voter.
   *
   * @param forTest            true if it's a test operation, false otherwise
   * @param protocolInstanceId the protocol instance's ID
   * @param voterId            the voter's unique ID
   *
   * @return the corresponding finalization log or null if none is found
   */
  @Transactional(readOnly = true)
  public FinalizationLogData fetchFinalizationLog(boolean forTest, String protocolInstanceId, int voterId) {
    final Integer operationId = operationService.checkOperationAccess(forTest, protocolInstanceId);
    final FinalizationLog finalizationLog = finalizationLogRepository.findByOperationIdAndVoterId(operationId, voterId);
    return Optional.ofNullable(finalizationLog).map(f -> new FinalizationLogData(f.getCode(), f.getDate()))
                   .orElse(null);
  }

  /**
   * Log a finalization vote.
   *
   * @param protocolInstanceId the protocol instance's ID
   * @param voterId            the voter's unique ID
   * @param code               the finalization code
   *
   * @return the newly created finalization log
   *
   * @throws EntityNotFoundException if the operation was not found
   */
  @Transactional(rollbackFor = Exception.class)
  public FinalizationLogData logFinalization(String protocolInstanceId, int voterId, String code) {
    OperationHolder operationHolder = operationService.getOperation(protocolInstanceId);

    final FinalizationLog finalizationLog = new FinalizationLog();
    finalizationLog.setOperation(operationHolder);
    finalizationLog.setVoterId(voterId);
    finalizationLog.setCode(code);
    finalizationLog.setDate(LocalDateTime.now());

    FinalizationLog savedLog = finalizationLogRepository.save(finalizationLog);
    return new FinalizationLogData(savedLog.getCode(), savedLog.getDate());
  }

  private <T> List<T> filterByDoi(List<? extends Data<T>> ballots,
                                  Function<T, String> doiExtractor,
                                  Set<String> voterDois) {
    return ballots.stream().map(jsonDocumentEntityService::convert)
                  .filter(ballot -> voterDois.contains(doiExtractor.apply(ballot)))
                  .collect(Collectors.toList());
  }

}
