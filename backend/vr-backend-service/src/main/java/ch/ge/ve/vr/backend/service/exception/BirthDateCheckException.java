/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

package ch.ge.ve.vr.backend.service.exception;

/**
 * Excheptions throwns in case of one of the birthdate checking
 */
public class BirthDateCheckException extends BusinessException {

  /**
   * Thrown when check failed
   *
   * @param messageKey: The message to explane reason
   * @param count:      number of checks
   * @param canRetry:   true if max count is not reached
   */
  public BirthDateCheckException(String messageKey, int count, boolean canRetry) {
    super(messageKey, String.valueOf(count), String.valueOf(canRetry));
  }

  /**
   * @param maxChecks: the max number of authorized checks
   *
   * @return ch.ge.ve.vr.backend.service.exception.BirthDateCheckException
   */
  public static BirthDateCheckException maxNumberOfChecksReached(int maxChecks) {
    return new BirthDateCheckException("max-number-of-birth-date-submitted", maxChecks, false);
  }

  /**
   * @param numberOfCheckLeft: remaining number of checks
   *
   * @return ch.ge.ve.vr.backend.service.exception.BirthDateCheckException
   */
  public static BirthDateCheckException wrongBirthDateSubmitted(int numberOfCheckLeft) {
    return new BirthDateCheckException("wrong-birth-date-submitted", numberOfCheckLeft, true);
  }

}
