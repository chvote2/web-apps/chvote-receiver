/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

package ch.ge.ve.vr.backend.service.asset;

import ch.ge.ve.vr.backend.repository.DoiCoatOfArmsRepository;
import ch.ge.ve.vr.backend.repository.RawFileRepository;
import ch.ge.ve.vr.backend.repository.entity.DoiCoatOfArms;
import ch.ge.ve.vr.backend.repository.entity.RawFile;
import ch.ge.ve.vr.backend.service.config.VoteReceiverConfigurationProperties;
import ch.ge.ve.vr.backend.service.exception.CoatOfArmsNotFoundException;
import ch.ge.ve.vr.backend.service.exception.RawFileNotFoundException;
import ch.ge.ve.vr.backend.service.exception.TranslationsNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.stereotype.Service;
import org.springframework.util.StreamUtils;

/**
 * Implementation class for the service managing assets
 */
@Service
public class AssetServiceImpl implements AssetService {

  private static final Logger  logger           = LoggerFactory.getLogger(AssetServiceImpl.class);
  private static final Pattern DOI_FILE_PATTERN = Pattern.compile("^([a-zA-Z0-9]+).*$");


  private final VoteReceiverConfigurationProperties voteReceiverConfig;
  private final PathMatchingResourcePatternResolver resolver;
  private final RawFileRepository                   rawFileRepository;
  private final DoiCoatOfArmsRepository             doiCoatOfArmsRepository;

  @Autowired
  public AssetServiceImpl(RawFileRepository rawFileRepository,
                          DoiCoatOfArmsRepository doiCoatOfArmsRepository,
                          VoteReceiverConfigurationProperties voteReceiverConfig,
                          PathMatchingResourcePatternResolver resolver) {
    this.rawFileRepository = rawFileRepository;
    this.doiCoatOfArmsRepository = doiCoatOfArmsRepository;
    this.voteReceiverConfig = voteReceiverConfig;
    this.resolver = resolver;
  }

  /**
   * Get transalation file corresponding to the protocol instance id
   *
   * @param protocolInstanceId: protocola instance id
   *
   * @return Transalation
   */
  @Override
  public String getTranslations(String protocolInstanceId) {
    return new String(rawFileRepository
                          .findTranslation(protocolInstanceId, voteReceiverConfig.getModelVersion())
                          .orElseThrow(() -> new TranslationsNotFoundException(protocolInstanceId)).getContent(),
                      StandardCharsets.UTF_8);
  }

  @Override
  public RawFile getFile(String protocolInstanceId, int fileIndex) {
    return rawFileRepository
        .findByOperation_ProtocolInstanceIdAndOperation_ModelVersionAndIndex(
            protocolInstanceId, voteReceiverConfig.getModelVersion(), fileIndex)
        .orElseThrow(() -> new RawFileNotFoundException(protocolInstanceId, fileIndex));
  }

  @Override
  public byte[] getDoiCoatOfArm(String id) {
    return doiCoatOfArmsRepository.findById(id)
                                  .map(DoiCoatOfArms::getContent)
                                  .orElseThrow(() -> new CoatOfArmsNotFoundException(id));
  }

  @PostConstruct
  @Transactional
  public void loadPredefinedDoiImages() throws IOException {
    Resource[] coatOfArmsFiles = resolver.getResources("classpath:doi-coat-of-arms/*");
    for (Resource file : coatOfArmsFiles) {
      @SuppressWarnings("ConstantConditions")
      Matcher doiFileMatcher = DOI_FILE_PATTERN.matcher(file.getFilename());
      if (doiFileMatcher.find()) {
        if (!doiCoatOfArmsRepository.findById(doiFileMatcher.group(1)).isPresent()) {
          DoiCoatOfArms doiCoatOfArms = new DoiCoatOfArms();
          doiCoatOfArms.setId(doiFileMatcher.group(1));
          doiCoatOfArms.setContent(StreamUtils.copyToByteArray(file.getInputStream()));
          doiCoatOfArmsRepository.save(doiCoatOfArms);
        }
      } else {
        logger.error("Unexpected file with in coat of arms directory {}", file.getFilename());
      }
    }
  }


}
