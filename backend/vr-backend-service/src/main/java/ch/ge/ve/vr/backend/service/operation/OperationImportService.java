/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

package ch.ge.ve.vr.backend.service.operation;

import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toMap;

import ch.ge.ve.chvote.pact.b2b.client.PactB2BClient;
import ch.ge.ve.chvote.pact.b2b.client.model.Documentation;
import ch.ge.ve.chvote.pact.b2b.client.model.Lang;
import ch.ge.ve.chvote.pact.b2b.client.model.OperationBaseConfiguration;
import ch.ge.ve.chvote.pact.b2b.client.model.VotationBallotConfig;
import ch.ge.ve.chvote.pact.b2b.client.model.Voter;
import ch.ge.ve.chvote.pact.b2b.client.model.VotingPeriod;
import ch.ge.ve.chvote.pact.b2b.client.model.VotingSiteConfiguration;
import ch.ge.ve.interfaces.ech.eCH0155.v4.AnswerInformationType;
import ch.ge.ve.interfaces.ech.eCH0155.v4.AnswerOptionIdentificationType;
import ch.ge.ve.interfaces.ech.eCH0155.v4.BallotDescriptionInformationType;
import ch.ge.ve.interfaces.ech.eCH0155.v4.BallotQuestionType;
import ch.ge.ve.interfaces.ech.eCH0155.v4.BallotType;
import ch.ge.ve.interfaces.ech.eCH0155.v4.QuestionInformationType;
import ch.ge.ve.interfaces.ech.eCH0155.v4.TieBreakInformationType;
import ch.ge.ve.interfaces.ech.eCH0155.v4.TieBreakQuestionType;
import ch.ge.ve.interfaces.ech.eCH0155.v4.VoteDescriptionInformationType;
import ch.ge.ve.interfaces.ech.eCH0159.v4.Delivery;
import ch.ge.ve.interfaces.ech.eCH0159.v4.EventInitialDelivery;
import ch.ge.ve.interfaces.ech.service.JAXBEchCodecImpl;
import ch.ge.ve.model.convert.model.AnswerTypeEnum;
import ch.ge.ve.protocol.model.Candidate;
import ch.ge.ve.protocol.model.DomainOfInfluence;
import ch.ge.ve.protocol.model.ElectionSetForVerification;
import ch.ge.ve.vr.backend.repository.OperationRepository;
import ch.ge.ve.vr.backend.repository.VoterRepository;
import ch.ge.ve.vr.backend.repository.data.Answer;
import ch.ge.ve.vr.backend.repository.data.BallotDocumentation;
import ch.ge.ve.vr.backend.repository.data.HighlightedQuestion;
import ch.ge.ve.vr.backend.repository.data.Operation;
import ch.ge.ve.vr.backend.repository.data.Subject;
import ch.ge.ve.vr.backend.repository.data.VariantSubjects;
import ch.ge.ve.vr.backend.repository.data.Votation;
import ch.ge.ve.vr.backend.repository.entity.Data;
import ch.ge.ve.vr.backend.repository.entity.OperationHolder;
import ch.ge.ve.vr.backend.repository.entity.RawFile;
import ch.ge.ve.vr.backend.repository.entity.VotationHolder;
import ch.ge.ve.vr.backend.repository.entity.VoterHolder;
import ch.ge.ve.vr.backend.service.config.VoteReceiverConfigurationProperties;
import ch.ge.ve.vr.backend.service.exception.CannotMatchCandidateToElectionSet;
import ch.ge.ve.vr.backend.service.exception.TechnicalException;
import ch.ge.ve.vr.backend.service.exception.TranslationsNotFoundException;
import ch.ge.ve.vr.backend.service.protocol.ProtocolService;
import ch.ge.ve.vr.backend.service.voter.VoterService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Preconditions;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import javax.transaction.Transactional;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service responsible of importing an operation coming from PACT into an Operation in Vote receiver
 */
@Service
public class OperationImportService {

  private static final String                              TRANSLATIONS_FILE_NAME   =
      "translations.json";
  private static final String                              SUBJECT_CANDIDATE_FORMAT = "%s:%s";
  private static final String                              BLANK_ANSWER             = "BLANK";
  private final        OperationRepository                 operationRepository;
  private final        VoterRepository                     voterRepository;
  private final        VoteReceiverConfigurationProperties config;
  private final        ObjectMapper                        objectMapper;
  private final        PactB2BClient                       pactClient;
  private final        ProtocolService                     protocolService;
  private final        VoterService                        voterService;
  private final        JAXBEchCodecImpl<Delivery>          votationCodec;

  @Autowired
  public OperationImportService(OperationRepository operationRepository,
                                VoterRepository voterRepository,
                                VoteReceiverConfigurationProperties config,
                                ObjectMapper objectMapper,
                                PactB2BClient pactClient,
                                ProtocolService protocolService,
                                VoterService voterService) {

    Properties defaultAnswerTranslation = new Properties();
    this.operationRepository = operationRepository;
    this.voterRepository = voterRepository;
    this.config = config;
    this.objectMapper = objectMapper;
    this.pactClient = pactClient;
    this.protocolService = protocolService;
    this.votationCodec = new JAXBEchCodecImpl<>(Delivery.class);
    this.voterService = voterService;

    try {
      defaultAnswerTranslation.load(
          new InputStreamReader(
              OperationImportService.class.getResourceAsStream("/default-answer-translations.properties"),
              StandardCharsets.UTF_8
          )
      );
    } catch (IOException e) {
      throw new TranslationsNotFoundException("Failed to load default answer translations : " + e.getMessage(), e);
    }
  }

  /**
   * Import or update an operation in the vote receiver based on the given protocol instance ID.
   * This import is idempotent: if the given protocol ID is already linked to a known operation in
   * the vote receiver, it will be silently ignored.
   * This import also handles model version changes by importing the operation with the new model
   * (while keeping the old one).
   *
   * @param protocolInstanceId the unique ID of the protocol instance linked to the operation
   */
  @Transactional
  public void importOperation(String protocolInstanceId) {
    // init file index
    AtomicInteger fileIndex = new AtomicInteger();

    // retrieve operation's configuration
    OperationBaseConfiguration operationConfiguration = pactClient.getOperationConfiguration(protocolInstanceId);

    // retrieve voting site's configuration
    VotingSiteConfiguration votingSiteConfiguration = pactClient.getVotingSiteConfiguration(protocolInstanceId);

    // map to an OperationHolder
    OperationHolder operationHolder = new OperationHolder();
    operationHolder.setModelVersion(config.getModelVersion());
    operationHolder.setCanton(operationConfiguration.getCanton().name());
    operationHolder.setLabel(Optional.ofNullable(operationConfiguration.getVotingCardLabel())
                                     .orElse(operationConfiguration.getLongLabel()));
    operationHolder.setDefaultLang(operationConfiguration.getDefaultLanguage());
    operationHolder.setClosingDate(
        Optional.ofNullable(operationConfiguration.getVotingPeriod()).map(VotingPeriod::getClosingDate).orElse(null));
    operationHolder.setOpeningDate(
        Optional.ofNullable(operationConfiguration.getVotingPeriod()).map(VotingPeriod::getOpeningDate).orElse(null));
    operationHolder.setGracePeriod(
        Optional.ofNullable(operationConfiguration.getVotingPeriod()).map(VotingPeriod::getGracePeriod).orElse(null));
    operationHolder.setDate(operationConfiguration.getOperationDate());
    operationHolder.setProtocolInstanceId(protocolInstanceId);
    operationHolder.setType(operationConfiguration.getType());

    // create raw files and corresponding documentation
    Set<RawFile> rawFiles = new HashSet<>();

    RawFile translations = createRawFile(operationHolder, rawFiles, fileIndex, TRANSLATIONS_FILE_NAME,
                                         votingSiteConfiguration.getTranslationsId());
    Map<Lang, Map<String, String>> answersTranslations = getTranslatedAnswers(translations.getContent());

    operationHolder.setTranslationFileIndex(translations.getIndex());

    // map to an Operation
    Operation operation = new Operation(
        operationConfiguration.getCanton().name(),
        protocolInstanceId,
        operationConfiguration.getType(),
        operationHolder.getLabel(),
        operationConfiguration.getSimulationName(),
        operationConfiguration.getDefaultLanguage(),
        votingSiteConfiguration.isGroupVotation(),
        translations.getIndex(),
        operationConfiguration.getVotingPeriod(),
        operationConfiguration.getOperationDate(),
        fetchBaseDocumentations(fileIndex, votingSiteConfiguration, operationHolder, rawFiles),
        fetchHighlightedQuestions(fileIndex, votingSiteConfiguration, operationHolder, rawFiles));


    ElectionSetForVerification electionSet = protocolService.getElectionSet(protocolInstanceId);

    addVotations(operationHolder, operationConfiguration, electionSet, votingSiteConfiguration, fileIndex, rawFiles,
                 answersTranslations);

    operationHolder.setElections(Collections.emptyList());

    attachData(operationHolder, operation);
    operationHolder.setRawFiles(rawFiles);

    // import voters only if this is a new operation
    if (operationRepository.findAllByProtocolInstanceId(protocolInstanceId).isEmpty()) {
      voterRepository.saveAll(createVoters(pactClient.getVoters(protocolInstanceId), protocolInstanceId));
    }

    operationRepository.save(operationHolder);
  }

  /**
   * @param translations json translation file content
   *
   * @return a Map containing anwer type and translation for the given Lang
   */
  private Map<Lang, Map<String, String>> getTranslatedAnswers(byte[] translations) {
    JSONObject jsonObject = new JSONObject(new String(translations, StandardCharsets.UTF_8));
    return EnumSet.allOf(Lang.class)
                  .stream()
                  .collect(Collectors.toMap(
                      lang -> lang,
                      lang -> {
                        JSONObject answers = jsonObject
                            .getJSONObject(lang.name().toLowerCase(Locale.US)).getJSONObject("ballot-list")
                            .getJSONObject("votation").getJSONObject("answers");
                        return answers.keySet().stream().collect(Collectors.toMap(key -> key, answers::getString));
                      }
                  ));

  }

  /**
   * Create a raw file for a documentation
   *
   * @param doc       The documentation
   * @param fileIndex The current file index (Will ve incremented)
   * @param operation The operation under construction
   * @param files     All the files created for this operation
   *
   * @return The new rawFile
   */
  private RawFile createRawFile(Documentation doc, AtomicInteger fileIndex, OperationHolder operation,
                                Set<RawFile> files) {
    return createRawFile(operation, files, fileIndex, doc.getLabel(), doc.getAttachmentId());
  }

  /**
   * Create a raw file for a documentation
   *
   * @param operation    The operation under construction
   * @param files        All the files created for this operation
   * @param index        The current file index (Will ve incremented)
   * @param name         The name of the file
   * @param attachmentId The attachment Id used to retrieve file content
   *
   * @return The new rawFile
   */
  private RawFile createRawFile(OperationHolder operation, Set<RawFile> files, AtomicInteger index, String name,
                                Long attachmentId) {
    RawFile rawFile = new RawFile();
    rawFile.setIndex(index.incrementAndGet());
    rawFile.setName(name);
    rawFile.setContent(fetchAttachmentContent(operation.getProtocolInstanceId(), attachmentId));
    rawFile.setOperation(operation);
    files.add(rawFile);
    return rawFile;
  }


  /**
   * Retrieve all general documentations
   *
   * @param fileIndex               The current file index (Will ve incremented)
   * @param votingSiteConfiguration The Site configuration
   * @param operationHolder         The operation under construction
   * @param rawFiles                All the files created for this operation
   *
   * @return a set of Documentation entity to create
   */
  private Set<ch.ge.ve.vr.backend.repository.data.Documentation> fetchBaseDocumentations(
      AtomicInteger fileIndex, VotingSiteConfiguration
      votingSiteConfiguration, OperationHolder operationHolder, Set<RawFile> rawFiles) {
    return votingSiteConfiguration
        .getDocumentations()
        .stream()
        .map(documentation -> new ch.ge.ve.vr.backend.repository.data.Documentation(
            documentation.getType(), documentation.getLabel(), documentation.getLang(),
            createRawFile(documentation, fileIndex, operationHolder, rawFiles).getIndex()
        )).collect(Collectors.toSet());
  }


  /**
   * Retrieve all highlighted questions
   *
   * @param fileIndex               The current file index (Will ve incremented)
   * @param votingSiteConfiguration The Site configuration
   * @param operationHolder         The operation under construction
   * @param rawFiles                All the files created for this operation
   *
   * @return a set of highlighted questions entity to create
   */
  private List<HighlightedQuestion> fetchHighlightedQuestions(
      AtomicInteger fileIndex, VotingSiteConfiguration votingSiteConfiguration, OperationHolder operationHolder,
      Set<RawFile> rawFiles) {
    return votingSiteConfiguration
        .getHighlightedQuestions()
        .stream()
        .map(hq -> new HighlightedQuestion(hq.getLabel(), hq.getLang(),
                                           createRawFile(hq, fileIndex, operationHolder, rawFiles).getIndex()))
        .collect(Collectors.toList());

  }

  /**
   * Retrieve an attachment on PACT
   *
   * @param protocolInstanceId protocol instance id
   * @param attachmentId       attachment id
   *
   * @return the content as a byte[]
   */
  private byte[] fetchAttachmentContent(String protocolInstanceId, Long attachmentId) {
    return pactClient.getAttachment(protocolInstanceId, attachmentId).getContent();
  }


  /**
   * Fetch votation information from eCH repository and protocol's election set
   *
   * @param operation               the operation holder
   * @param operationConfiguration  the operation's configuration
   * @param electionSet             the protocol's election set for this operation
   * @param votingSiteConfiguration the voting site configuration
   * @param fileIndex               current file index for the raw files
   * @param rawFiles                the list of raw files
   * @param answersTranslations     default translations of votation answer
   */
  private void addVotations(OperationHolder operation,
                            OperationBaseConfiguration operationConfiguration,
                            ElectionSetForVerification electionSet,
                            VotingSiteConfiguration votingSiteConfiguration,
                            AtomicInteger fileIndex,
                            Set<RawFile> rawFiles,
                            Map<Lang, Map<String, String>> answersTranslations) {

    Map<String, VotationBallotConfig> ballotsConfig = votingSiteConfiguration
        .getVotationBallotConfigList()
        .stream()
        .collect(toMap(VotationBallotConfig::getVotationBallotId, identity()));

    final Map<String, Integer> indexedCandidates = getCandidateIndexes(electionSet.getCandidates());
    operation.setVotations(
        operationConfiguration
            .getVotationRepositoryIds()
            .stream()
            .map(id -> pactClient.getAttachment(operation.getProtocolInstanceId(), id).getContent())
            .flatMap(this::getVoteInformation)
            .map(voteInformation -> {
              VotationHolder votationHolder = new VotationHolder();
              votationHolder.setOperation(operation);
              Votation votation = new Votation(
                  voteInformation.getVote().getDomainOfInfluenceIdentification(),
                  getVotationLocalizedLabel(voteInformation),
                  fetchBallotDocumentation(operation, fileIndex, rawFiles, ballotsConfig, voteInformation),
                  createSimpleSubjects(voteInformation, indexedCandidates, answersTranslations),
                  createVariantSubject(voteInformation, indexedCandidates, answersTranslations));
              attachData(votationHolder, votation);
              return votationHolder;
            })
            .collect(Collectors.toList()));
  }

  private List<VariantSubjects> createVariantSubject(EventInitialDelivery.VoteInformation voteInformation,
                                                     Map<String, Integer> indexedCandidates,
                                                     Map<Lang, Map<String, String>> answersTranslations) {
    return voteInformation
        .getBallot()
        .stream().filter(ballotType -> ballotType.getVariantBallot() != null)
        .map((BallotType ballotType) -> convertVariantSubjects(
            ballotType.getBallotGroup(), ballotType.getVariantBallot(), indexedCandidates, answersTranslations))
        .collect(Collectors.toList());
  }

  private List<Subject> createSimpleSubjects(
      EventInitialDelivery.VoteInformation voteInformation, Map<String, Integer> indexedCandidates,
      Map<Lang, Map<String, String>> answersTranslations) {
    return voteInformation
        .getBallot().stream()
        .filter(ballotType -> ballotType.getStandardBallot() != null)
        .map(ballotType -> convertStandardBallot(ballotType.getStandardBallot(), indexedCandidates,
                                                 answersTranslations)).collect(Collectors.toList());
  }

  private Set<BallotDocumentation> fetchBallotDocumentation(
      OperationHolder operationHolder, AtomicInteger fileIndex, Set<RawFile> rawFiles, Map<String,
      VotationBallotConfig> ballotsConfig, EventInitialDelivery.VoteInformation voteInformation) {
    return Optional
        .ofNullable(ballotsConfig.get(voteInformation.getVote().getVoteIdentification()))
        .stream()
        .flatMap(vbc -> vbc
            .getBallotDocumentations()
            .stream()
            .map(doc -> new BallotDocumentation(doc.getLabel(), doc.getLang(),
                                                createRawFile(doc, fileIndex, operationHolder, rawFiles).getIndex())
            )).collect(Collectors.toSet());
  }

  private Map<Lang, String> getVotationLocalizedLabel(EventInitialDelivery.VoteInformation voteInformation) {
    Map<Lang, String> localizedLabel = new EnumMap<>(Lang.class);
    Optional.ofNullable(voteInformation.getVote().getVoteDescription())
            .map(VoteDescriptionInformationType::getVoteDescriptionInfo)
            .ifPresent(infos -> infos.forEach(
                info ->
                    localizedLabel.put(
                        Lang.getLanguageByLanguageTag(info.getLanguage()),
                        info.getVoteDescription())));
    return localizedLabel;
  }

  private Stream<EventInitialDelivery.VoteInformation> getVoteInformation(byte[] content) {
    return votationCodec.deserialize(new ByteArrayInputStream(content))
                        .getInitialDelivery()
                        .getVoteInformation()
                        .stream();
  }

  private Map<String, Integer> getCandidateIndexes(List<Candidate> candidates) {
    return IntStream.range(0, candidates.size()).boxed()
                    .collect(Collectors.toMap(i -> candidates.get(i).getCandidateDescription(), i -> i + 1));
  }

  private <T> void attachData(Data<T> entity, T data) {
    try {
      entity.setData(objectMapper.writeValueAsString(data));
    } catch (JsonProcessingException e) {
      throw new TechnicalException(e);
    }
  }

  private Set<VoterHolder> createVoters(Collection<Voter> voters, String protocolInstanceId) {
    return voters.stream().map(voter -> {
      VoterHolder voterHolder = new VoterHolder();
      voterHolder.setProtocolInstanceId(protocolInstanceId);

      voterHolder.setVoterBirthDateScope(BirthDateScopeMapper.mapToScope(voter.getVoterBirthDay(), voter.getVoterBirthMonth()));

      voterHolder.setVoterBirthHashed(
          voterService.hashDateOfBirth(
              voter.getVoterBirthDay(), voter.getVoterBirthMonth(), voter.getVoterBirthYear()));
      voterHolder.setVoterId(voter.getVoterIndex());
      voterHolder.setDomainOfInfluences(voter.getDomainOfInfluenceIds()
                                             .stream()
                                             .map(DomainOfInfluence::getIdentifier)
                                             .collect(Collectors.toSet()));
      return voterHolder;
    }).collect(Collectors.toSet());
  }


  private Subject convertStandardBallot(BallotType.StandardBallot ballot, Map<String, Integer> indexedCandidates, Map
      <Lang, Map<String, String>> votationAnswersTranslations) {
    return new Subject(ballot.getBallotQuestionNumber(),
                       fetchSubjectLabel(ballot.getBallotQuestion()),
                       convertAnswers(ballot.getAnswerInformation(), indexedCandidates,
                                      ballot.getQuestionIdentification(), votationAnswersTranslations));
  }

  private VariantSubjects convertVariantSubjects(
      BallotDescriptionInformationType ballotGroup, BallotType
      .VariantBallot ballot,
      Map<String, Integer> indexedCandidates, Map<Lang, Map<String, String>> answersTranslations) {
    Preconditions.checkArgument(ballot.getTieBreakInformation().size() == 1, "should have 1 tie break");
    Preconditions.checkArgument(ballot.getQuestionInformation().size() >= 2, "should have at least 2 subjects");

    return new VariantSubjects(
        fetchBallotGroupLabel(ballotGroup),
        ballot.getQuestionInformation().stream()
              .map(questionInformation -> convertQuestionInformationToSubject(questionInformation, indexedCandidates,
                                                                              answersTranslations))
              .collect(Collectors.toList()),
        convertTieBreakToSubject(ballot.getTieBreakInformation().get(0), indexedCandidates, answersTranslations));
  }

  private Subject convertQuestionInformationToSubject(
      QuestionInformationType questionInformation, Map<String, Integer> indexedCandidates,
      Map<Lang, Map<String, String>> votationAnswersTranslations) {

    return new Subject(
        questionInformation.getBallotQuestionNumber(),
        fetchSubjectLabel(questionInformation.getBallotQuestion()),
        convertAnswers(questionInformation.getAnswerInformation(), indexedCandidates,
                       questionInformation.getQuestionIdentification(), votationAnswersTranslations));
  }

  private Subject convertTieBreakToSubject(
      TieBreakInformationType tieBreakInformationType, Map<String, Integer> indexedCandidates,
      Map<Lang, Map<String, String>> answersTranslations) {

    return new Subject(
        tieBreakInformationType.getTieBreakQuestionNumber(),
        fetchSubjectLabel(tieBreakInformationType.getTieBreakQuestion()),
        convertAnswers(tieBreakInformationType.getAnswerInformation(), indexedCandidates,
                       tieBreakInformationType.getQuestionIdentification(), answersTranslations));
  }

  private Map<Lang, String> fetchBallotGroupLabel(BallotDescriptionInformationType ballotDescriptionInformationType) {
    return ballotDescriptionInformationType.getBallotDescriptionInfo().stream()
                                           .collect(toMap(b -> Lang.getLanguageByLanguageTag(b.getLanguage()),
                                                          BallotDescriptionInformationType
                                                              .BallotDescriptionInfo::getBallotDescriptionLong));
  }

  private Map<Lang, String> fetchSubjectLabel(BallotQuestionType ballotQuestion) {
    return ballotQuestion.getBallotQuestionInfo().stream()
                         .collect(toMap(b -> Lang.getLanguageByLanguageTag(b.getLanguage()),
                                        BallotQuestionType.BallotQuestionInfo::getBallotQuestion));
  }

  private Map<Lang, String> fetchSubjectLabel(TieBreakQuestionType tieBreakQuestion) {
    return tieBreakQuestion.getTieBreakQuestionInfo().stream()
                           .collect(toMap(q -> Lang.getLanguageByLanguageTag(q.getLanguage()),
                                          TieBreakQuestionType.TieBreakQuestionInfo::getTieBreakQuestion));
  }

  private List<Answer> convertAnswers(AnswerInformationType answerType, Map<String, Integer> indexedCandidates,
                                      String questionIdentification,
                                      Map<Lang, Map<String, String>> votationAnswersTranslations) {

    AnswerTypeEnum answerTypeEnum = AnswerTypeEnum.fromInt(answerType.getAnswerType().intValue());
    List<Answer> answers = new ArrayList<>(answerTypeEnum.getOptions().size());
    for (int i = 0; i < answerTypeEnum.getOptions().size(); i++) {
      String optionName = answerTypeEnum.getOptions().get(i);
      final String subjectCandidate = String.format(SUBJECT_CANDIDATE_FORMAT, questionIdentification, optionName);
      Integer protocolIndex = indexedCandidates.get(subjectCandidate);
      if (protocolIndex == null) {
        throw new CannotMatchCandidateToElectionSet(subjectCandidate, indexedCandidates.keySet());
      }
      answers.add(new Answer(protocolIndex,
                             BLANK_ANSWER.equals(optionName),
                             fetchAnswerLocalizedLabel(answerType, optionName, i + 1, votationAnswersTranslations)));
    }
    return answers;
  }

  private Map<Lang, String> fetchAnswerLocalizedLabel(
      AnswerInformationType answerType, String optionName, int answerSequenceNumber,
      Map<Lang, Map<String, String>> votationAnswersTranslations) {


    Map<Lang, String> answerLocalizedLabel = getDefaultAnswerLocalizedLabel(optionName, votationAnswersTranslations);
    answerType.getAnswerOptionIdentification()
              .stream()
              .filter(answerOptionIdentificationType ->
                          answerOptionIdentificationType.getAnswerSequenceNumber().intValue() ==
                          answerSequenceNumber).findFirst()
              .map(AnswerOptionIdentificationType::getAnswerTextInformation)
              .ifPresent(answerTextInformations -> answerTextInformations.forEach(
                  answerTextInformation ->
                      answerLocalizedLabel.put(Lang.getLanguageByLanguageTag(answerTextInformation.getLanguage()),
                                               answerTextInformation.getAnswerText())
              ));
    return answerLocalizedLabel;
  }

  private Map<Lang, String> getDefaultAnswerLocalizedLabel(
      String optionName, Map<Lang, Map<String, String>> votationAnswersTranslations) {
    return votationAnswersTranslations
        .entrySet().stream().collect(
            Collectors.toMap(
                Map.Entry::getKey,
                e -> Optional.ofNullable(e.getValue().get(optionName.toLowerCase(Locale.US)))
                             .orElseThrow(() -> new IllegalArgumentException(
                                 String.format("could not find default translation for option with name %s on lang %s",
                                               optionName, e.getKey().name())))
            )
        );
  }
}
