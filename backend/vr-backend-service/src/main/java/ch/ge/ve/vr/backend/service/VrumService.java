/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

package ch.ge.ve.vr.backend.service;

import ch.ge.ve.vr.backend.service.config.VrumConfiguration;
import ch.ge.ve.vr.backend.service.exception.VrumIdentificationFailure;
import ch.ge.ve.vr.backend.service.vrum.model.IdentificationStatus;
import java.text.MessageFormat;
import org.apache.http.client.fluent.Executor;
import org.apache.http.client.fluent.Request;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * Vote Right Uage Manager (VRUM) service
 */
@Service
public class VrumService {
  private static final Logger            logger = LoggerFactory.getLogger(VrumService.class);
  private final        VrumConfiguration vrumConfiguration;
  private final        Executor          executor;

  public VrumService(VrumConfiguration vrumConfiguration, Executor executor) {
    this.vrumConfiguration = vrumConfiguration;
    this.executor = executor;
  }

  /**
   * Check voter rights
   *
   * @param protocolInstanceId: protocol instance unique ID
   * @param voterId:            voter identifier
   */
  public void assertCanIdentify(String protocolInstanceId, int voterId) {
    IdentificationStatus status;
    try {
      status =
          IdentificationStatus.valueOf(executor.execute(RestServiceUtils.attachCredentialsToRequest(
              Request.Get(MessageFormat.format(vrumConfiguration.getCanIdentifyUri(), protocolInstanceId, voterId)),
              vrumConfiguration.getUsername(), vrumConfiguration.getPassword())
          ).returnContent().asString());
    } catch (Exception e) {
      logger.error(
          "Error on calling VRUM for identification on voter " + voterId + " and protocol instance Id " +
          protocolInstanceId, e);
      throw VrumIdentificationFailure.unexpectedFailure();
    }

    if (status != IdentificationStatus.READY_TO_VOTE) {
      throw VrumIdentificationFailure.cannotVote(status);
    }
  }

}
