/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

package ch.ge.ve.vr.backend.service.model;

import ch.ge.ve.chvote.pact.b2b.client.model.Lang;
import ch.ge.ve.chvote.pact.b2b.client.model.VotingPeriod;
import java.time.LocalDateTime;

/**
 * Base information regarding an operation.
 */
public class OperationSummary {

  private final String        label;
  private final String        protocolInstanceId;
  private final Lang          defaultLang;
  private final VotingPeriod  votingPeriod;
  private final LocalDateTime date;

  public OperationSummary(String label, String protocolInstanceId, Lang defaultLang, VotingPeriod votingPeriod,
                          LocalDateTime date) {
    this.label = label;
    this.protocolInstanceId = protocolInstanceId;
    this.defaultLang = defaultLang;
    this.votingPeriod = votingPeriod;
    this.date = date;
  }

  public String getLabel() {
    return label;
  }

  public String getProtocolInstanceId() {
    return protocolInstanceId;
  }

  public Lang getDefaultLang() {
    return defaultLang;
  }

  public VotingPeriod getVotingPeriod() {
    return votingPeriod;
  }

  public LocalDateTime getDate() {
    return date;
  }
}
