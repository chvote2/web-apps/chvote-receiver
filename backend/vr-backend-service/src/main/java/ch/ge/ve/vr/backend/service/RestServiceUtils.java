/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

package ch.ge.ve.vr.backend.service;

import java.nio.charset.Charset;
import java.util.Base64;
import org.apache.http.client.fluent.Request;
import org.springframework.util.StringUtils;

final class RestServiceUtils {

  private RestServiceUtils() {

  }


  /**
   * Add needed credentials for pact
   */
  static Request attachCredentialsToRequest(Request request, String pactUsername, String pactPassword) {
    if (StringUtils.isEmpty(pactUsername)) {
      return request;
    }

    return request
        .addHeader("X-Requested-With", "XMLHttpRequest")
        .addHeader("Authorization",
                   "Basic " + Base64.getEncoder().encodeToString(
                       (pactUsername + ":" + pactPassword).getBytes(Charset.forName("UTF-8"))));
  }

}
