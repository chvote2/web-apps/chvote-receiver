/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

package ch.ge.ve.vr.backend.service.model;

import ch.ge.ve.vr.backend.repository.data.Election;
import ch.ge.ve.vr.backend.repository.data.Votation;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

/**
 * Operation's data linked to one voter.
 */
public class VoterOperationData {
  private final List<Votation> votations;
  private final List<Election> elections;
  private final BirthDateScope birthDateScope;

  @JsonCreator
  public VoterOperationData(@JsonProperty("votations") List<Votation> votations,
                            @JsonProperty("elections") List<Election> elections,
                            @JsonProperty("birthDateScope") BirthDateScope birthDateScope) {
    this.votations = votations;
    this.elections = elections;
    this.birthDateScope = birthDateScope;
  }

  public List<Votation> getVotations() {
    return votations;
  }

  public List<Election> getElections() {
    return elections;
  }

  public BirthDateScope getBirthDateScope() {
    return birthDateScope;
  }
}
