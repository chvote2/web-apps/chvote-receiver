/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

package ch.ge.ve.vr.backend.service.operation;

import ch.ge.ve.chvote.pact.b2b.client.exceptions.ProtocolInstanceNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Listen for operations change comming from PACT
 */
@Service
public class PactOperationListener {
  private static final Logger                  logger = LoggerFactory.getLogger(PactOperationListener.class);
  private final        OperationImportService  importService;
  private final        OperationDestroyService destroyService;

  /**
   * Base constructor,
   *
   * @param importService  service to save the retrieved operations
   * @param destroyService service to remove an operation
   */
  @Autowired
  public PactOperationListener(OperationImportService importService, OperationDestroyService destroyService) {
    this.importService = importService;
    this.destroyService = destroyService;
  }

  /**
   * Handles a new operation notification by retrieving operation's information from PACT
   * and saving a new operation instance in the vote receiver.
   *
   * @param protocolInstanceId the protocol instance's id linked to the new operation
   *
   * @throws ProtocolInstanceNotFoundException                                      if no corresponding protocol
   *                                                                                instance exists
   * @throws ch.ge.ve.chvote.pact.b2b.client.exceptions.AttachmentNotFoundException if an expected attachment is not
   *                                                                                available
   */
  public void newOperation(String protocolInstanceId) {
    logger.info("new or update operation message {}", protocolInstanceId);
    importService.importOperation(protocolInstanceId);
  }

  /**
   * Handles a destroy operation notification by deleting information relative to this operation
   *
   * @param protocolInstanceId the protocol instance's id linked to the destroyed operation
   */
  public void destroyOperation(String protocolInstanceId) {
    logger.info("destroy operation message {}", protocolInstanceId);
    destroyService.destroyInstance(protocolInstanceId);
  }

}
