/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

package ch.ge.ve.vr.backend.service;

import ch.ge.ve.vr.backend.repository.entity.Data;
import ch.ge.ve.vr.backend.service.exception.TechnicalException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Technical service used to convert entities to json object
 */

@Service
public class JsonDocumentEntityService {
  private final ObjectMapper objectMapper;

  @Autowired
  public JsonDocumentEntityService(ObjectMapper objectMapper) {
    this.objectMapper = objectMapper;
  }

  public <U, T extends Data<U>> U convert(T entity) {
    try {
      return objectMapper.readValue(entity.getData(), entity.getDataClass());
    } catch (IOException e) {
      throw new TechnicalException(e);
    }

  }
}
