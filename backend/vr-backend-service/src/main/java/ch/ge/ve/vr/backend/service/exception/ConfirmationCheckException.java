/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

package ch.ge.ve.vr.backend.service.exception;

/**
 * Exception thrown when an error occurs during confirmation code submission
 */
public class ConfirmationCheckException extends BusinessException {

  public ConfirmationCheckException(String messageKey, int count, boolean canRetry) {
    super(messageKey, String.valueOf(count), String.valueOf(canRetry));
  }

  public static ConfirmationCheckException maxNumberOfChecksReached(int maxChecks) {
    return new ConfirmationCheckException("max-number-of-confirmation-reached", maxChecks, false);
  }

  public static ConfirmationCheckException wrongConfirmationCodeSubmitted(int numberOfCheckLeft) {
    return new ConfirmationCheckException("wrong-confirmation-code-submitted", numberOfCheckLeft, true);
  }

}
