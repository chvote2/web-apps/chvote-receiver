/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

package ch.ge.ve.vr.backend.mock.server;

import ch.ge.ve.jacksonserializer.BigIntegerAsBase64Deserializer;
import ch.ge.ve.jacksonserializer.BigIntegerAsBase64Serializer;
import ch.ge.ve.jacksonserializer.JSDates;
import ch.ge.ve.vr.backend.service.config.VoteReceiverConfigurationProperties;
import java.io.IOException;
import java.security.Security;
import java.util.TimeZone;
import javax.annotation.PostConstruct;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootApplication
@Import({SwaggerConfig.class, VoteReceiverConfigurationProperties.class})
@EnableTransactionManagement
@EnableAspectJAutoProxy
@ComponentScan(basePackages = {"ch.ge.ve.vr.backend.mock.server", "ch.ge.ve.vr.backend.service.voter"})
@EnableJpaRepositories(basePackages = "ch.ge.ve.vr.backend.repository")
@EntityScan(basePackages = "ch.ge.ve.vr.backend.repository.entity")
public class MockServer implements WebMvcConfigurer {
  static final String VOTE_RECEIVER_EXCHANGE = "vote-receiver-exchange";

  @Value("${env.timezone}")
  private String timeZone;

  public static void main(String[] args) {
    Security.addProvider(new BouncyCastleProvider());
    SpringApplication.run(MockServer.class, args);
  }


  @Bean
  public Jackson2ObjectMapperBuilderCustomizer jackson2ObjectMapperBuilderCustomizer() {
    return jacksonObjectMapperBuilder -> {
      jacksonObjectMapperBuilder.serializers(JSDates.SERIALIZER);
      jacksonObjectMapperBuilder.deserializers(JSDates.DESERIALIZER);
      jacksonObjectMapperBuilder.serializers(new BigIntegerAsBase64Serializer());
      jacksonObjectMapperBuilder.deserializers(new BigIntegerAsBase64Deserializer());
    };
  }

  @PostConstruct
  public void setDefaultTimezone() {
    TimeZone.setDefault(TimeZone.getTimeZone(timeZone));
  }

  @Bean
  public Filter addNoCacheFilter() {
    return new OncePerRequestFilter() {
      @Override
      protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
          throws ServletException, IOException {
        response.addHeader("Cache-Control", "no-cache, no-store, must-revalidate");
        response.addHeader("pragma", "no-cache");
        filterChain.doFilter(request, response);
      }
    };
  }
}