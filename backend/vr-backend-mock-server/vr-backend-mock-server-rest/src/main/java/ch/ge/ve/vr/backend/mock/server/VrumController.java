/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

package ch.ge.ve.vr.backend.mock.server;

import ch.ge.ve.vr.backend.service.vrum.model.IdentificationStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class VrumController {
  private final VrumSimulatorService simulatorService;

  @Autowired
  public VrumController(VrumSimulatorService simulatorService) {
    this.simulatorService = simulatorService;
  }


  @GetMapping(path = "vrum/can-identify")
  ResponseEntity canIdentify(@RequestParam(value = "protocolInstanceId") String protocolInstanceId,
                             @RequestParam(value = "voterId") String voterId) {

    return ResponseEntity.ok()
                         .contentType(MediaType.TEXT_PLAIN)
                         .body(simulatorService.canIdentify(protocolInstanceId, voterId).name());

  }

  @GetMapping(path = "vrum/set-status")
  void setStatus(@RequestParam(value = "protocolInstanceId") String protocolInstanceId,
                 @RequestParam(value = "voterId") String voterId,
                 @RequestParam(value = "status") IdentificationStatus status) {
    simulatorService.setVoterStatus(protocolInstanceId, voterId, status);
  }

}
