/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

package ch.ge.ve.vr.backend.mock.server;

import ch.ge.ve.vr.backend.service.vrum.model.IdentificationStatus;
import java.util.HashMap;
import java.util.Map;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;

@Service
public class VrumSimulatorService {

  private final Map<Pair<String, String>, IdentificationStatus> voterStatuses = new HashMap<>();


  public IdentificationStatus canIdentify(String protocolInstanceId, String voterId) {
    return voterStatuses
        .computeIfAbsent(Pair.of(protocolInstanceId, voterId), pair -> IdentificationStatus.READY_TO_VOTE);
  }

  public void setVoterStatus(String protocolInstanceId, String voterId, IdentificationStatus identificationStatus) {
    voterStatuses.put(Pair.of(protocolInstanceId, voterId), identificationStatus);
  }


  public void clear() {
    voterStatuses.clear();
  }
}
