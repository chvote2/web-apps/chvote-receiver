/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

package ch.ge.ve.vr.backend.repository;

import ch.ge.ve.vr.backend.repository.entity.OperationHolder;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import javax.persistence.LockModeType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Repository interface to handle {@link OperationHolder} entities.
 */
@Repository
public interface OperationRepository extends JpaRepository<OperationHolder, Integer> {

  /**
   * Retrieves all the operations linked to a given protocol instance ID.
   *
   * @param protocolInstanceId the protocol instance unique ID
   *
   * @return the liste of operations linked to this protocol
   */
  List<OperationHolder> findAllByProtocolInstanceId(String protocolInstanceId);


  @Query("select o from OperationHolder o where o.modelVersion = :modelVersion " +
         "and o.canton = :canton and (o.type = 'TEST' or o.type= 'SIMULATION' and o.closingDate > :maxDate)")
  List<OperationHolder> findAllForTestMode(@Param("modelVersion") int modelVersion,
                                           @Param("canton") String canton,
                                           @Param("maxDate") LocalDateTime maxDate);

  @Query("select o from OperationHolder o where o.modelVersion = :modelVersion " +
         "and o.canton = :canton and o.type= 'REAL' and o.closingDate > :maxDate")
  List<OperationHolder> findAllForRealMode(@Param("modelVersion") int modelVersion,
                                           @Param("canton") String canton,
                                           @Param("maxDate") LocalDateTime maxDate);

  @Lock(LockModeType.PESSIMISTIC_WRITE)
  @Query("select o from OperationHolder o where o.modelVersion < :modelVersion " +
         "and exists (select o2 from OperationHolder o2 where" +
         " o.protocolInstanceId = o2.protocolInstanceId and" +
         " o.modelVersion = o2.modelVersion)")
  List<OperationHolder> findOperationsForUpdate(@Param("modelVersion") int modelVersion);

  Optional<OperationHolder> findByProtocolInstanceIdAndModelVersion(String protocolInstanceId, int modelVersion);
}
