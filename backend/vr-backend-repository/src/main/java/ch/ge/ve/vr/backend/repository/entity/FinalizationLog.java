/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

package ch.ge.ve.vr.backend.repository.entity;

import java.time.LocalDateTime;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Entity containing a voting finalization log
 */
@Entity
@Table(name = "VR_T_FINALIZATION_LOG")
public class FinalizationLog {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "FLG_N_ID", nullable = false)
  private Integer id;

  @Column(name = "FLG_N_VOTER_ID", nullable = false)
  private int voterId;

  @Column(name = "FLG_C_CODE", nullable = false)
  private String code;

  @Column(name = "FLG_D_DATE", nullable = false)
  private LocalDateTime date;

  @ManyToOne(optional = false)
  @JoinColumn(name = "FLG_OPE_N_ID", referencedColumnName = "OPE_N_ID")
  private OperationHolder operation;

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public int getVoterId() {
    return voterId;
  }

  public void setVoterId(int voterId) {
    this.voterId = voterId;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public LocalDateTime getDate() {
    return date;
  }

  public void setDate(LocalDateTime date) {
    this.date = date;
  }

  public OperationHolder getOperation() {
    return operation;
  }

  public void setOperation(OperationHolder operation) {
    this.operation = operation;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof FinalizationLog)) {
      return false;
    }
    FinalizationLog that = (FinalizationLog) o;
    return Objects.equals(voterId, that.voterId) &&
           Objects.equals(operation, that.operation);
  }

  @Override
  public int hashCode() {
    return Objects.hash(voterId, operation);
  }
}
