/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

package ch.ge.ve.vr.backend.repository.data;

import ch.ge.ve.chvote.pact.b2b.client.model.Lang;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Map;

/**
 * Votation ballot subject's answer.
 */
public class Answer {

  private final int               protocolIndex;
  private final boolean           virtual;
  private final Map<Lang, String> localizedLabel;

  @JsonCreator
  public Answer(@JsonProperty("protocolIndex") int protocolIndex,
                @JsonProperty("virtual") boolean virtual,
                @JsonProperty("localizedLabel") Map<Lang, String> localizedLabel) {
    this.protocolIndex = protocolIndex;
    this.virtual = virtual;
    this.localizedLabel = localizedLabel;
  }

  public int getProtocolIndex() {
    return protocolIndex;
  }

  public boolean isVirtual() {
    return virtual;
  }

  public Map<Lang, String> getLocalizedLabel() {
    return localizedLabel;
  }
}
