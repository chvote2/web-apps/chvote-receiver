/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

package ch.ge.ve.vr.backend.repository.entity;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

/**
 * Entity containing voter configuration for a given operation.
 */
@Entity
@Table(name = "VR_T_VOTER")
public class VoterHolder {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "VTR_N_ID", nullable = false)
  private Long id;

  @Column(name = "VTR_C_PROTOCOL_ID", nullable = false)
  private String protocolInstanceId;

  @Column(name = "VTR_N_VOTER_ID", nullable = false)
  private int voterId;

  @ElementCollection
  @CollectionTable(name = "VR_T_VOTER_DOI", joinColumns = {@JoinColumn(name = "VTR_N_ID")})
  @Column(name = "VOD_C_DOI")
  private Set<String> domainOfInfluences = new HashSet<>();

  @Column(name = "VTR_C_BIRTH_HASH", nullable = false)
  private String voterBirthHashed;

  @Enumerated(EnumType.STRING)
  @Column(name = "VTR_N_BIRTH_SCOPE", nullable = false)
  private BirthDateScope voterBirthDateScope;

  @Column(name = "VTR_N_BIRTH_DAY_CHECK_FAILED")
  private Integer birthDayCheckFailed = 0;

  @Column(name = "VTR_N_CONFIRMATION_CHECK_FAILED")
  private Integer confirmationCheckFailed = 0;

  @Column(name = "VTR_N_VERIFICATION_CODE_REVEALED")
  private boolean verificationCodeRevealed;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getProtocolInstanceId() {
    return protocolInstanceId;
  }

  public void setProtocolInstanceId(String protocolInstanceId) {
    this.protocolInstanceId = protocolInstanceId;
  }

  public int getVoterId() {
    return voterId;
  }

  public void setVoterId(int voterId) {
    this.voterId = voterId;
  }


  public Set<String> getDomainOfInfluences() {
    return domainOfInfluences;
  }

  public void setDomainOfInfluences(Set<String> domainOfInfluences) {
    this.domainOfInfluences = domainOfInfluences;
  }

  public String getVoterBirthHashed() {
    return voterBirthHashed;
  }

  public void setVoterBirthHashed(String voterBirthHashed) {
    this.voterBirthHashed = voterBirthHashed;
  }

  public BirthDateScope getVoterBirthDateScope() {
    return voterBirthDateScope;
  }

  public void setVoterBirthDateScope(BirthDateScope voterBirthDateScope) {
    this.voterBirthDateScope = voterBirthDateScope;
  }

  public Integer getBirthDayCheckFailed() {
    return birthDayCheckFailed;
  }

  public void setBirthDayCheckFailed(Integer birthDayCheckFailed) {
    this.birthDayCheckFailed = birthDayCheckFailed;
  }

  public Integer getConfirmationCheckFailed() {
    return confirmationCheckFailed;
  }

  public void setConfirmationCheckFailed(Integer confirmationCheckFailed) {
    this.confirmationCheckFailed = confirmationCheckFailed;
  }

  public void setVerificationCodeRevealed(boolean verificationCodeRevealed) {
    this.verificationCodeRevealed = verificationCodeRevealed;
  }

  public boolean isVerificationCodeRevealed() {
    return verificationCodeRevealed;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    VoterHolder that = (VoterHolder) o;
    return voterId == that.voterId &&
           id.equals(that.id) &&
           voterBirthHashed.equals(that.voterBirthHashed) &&
           voterBirthDateScope == that.voterBirthDateScope;
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, voterId, voterBirthHashed, voterBirthDateScope);
  }

  public enum BirthDateScope {
    YEAR_MONTH_DAY,
    YEAR_MONTH,
    YEAR
  }
}
